1. All members (Hubert Barc, Maciej Długoszek, Andżelika Domańska, Kacper Drejer, Beata Kakareko) accept specification.

2. Project methodology: Scrum.

3. Roles:
- Hubert Barc - Product Owner
- Maciej Długoszek - Developer
- Andżelika Domańska - Developer
- Kacper Drejer - Developer
- Beata Kakareko - Scrum Muster

4. Project Structure
Solution contains the following projects:
- Core Project - common for all components
- CommunicationServer Project
- GameMaster Project
- Player Project
- UnitTest Project
- UI project

5. Name conventions
- issues => tasks on JIRA
<simple and clear description>
example: "Board initialization"
- branches
<issue number>-<feat/bug/hotfix>-<issue_description>  
example: "24-feat-player_actions"
- commits
<add/fix/change/etc..>:<description>
example: "add: place and destroy piece actions"


Documentation: https://bitbucket.org/okulwut/mini-softwareengineering-theprojectgame/downloads/

