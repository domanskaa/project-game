from subprocess import Popen
import time
processes = []

p = Popen(["dotnet", "./ProjectGame.GameMaster/bin/Debug/netcoreapp2.0/ProjectGame.GameMaster.dll", "--address", "127.0.0.1", "--port", "4444", "--conf", "./ProjectGame.UnitTests/XML/Configuration.xml"], stdout=open("gm_output.log.txt", "w"))
processes.append(p)
time.sleep(2)


for role in ["member"]*1 + ["leader"]:
	for team in ["red", "blue"]:
		p = Popen(["dotnet", "./ProjectGame.Player/bin/Debug/netcoreapp2.0/ProjectGame.PlayerProject.dll", "--address", "127.0.0.1", "--port", "4444", "--game", "Endgame", "--team", team, "--role", role, "--conf", "./ProjectGame.UnitTests/XML/PlayerConfiguration.xml"], stdout=open(role + "-" + team + ".log.txt", "w"))
		processes.append(p)


time.sleep(20)
p.kill()
for process in processes:
	process.wait()
		