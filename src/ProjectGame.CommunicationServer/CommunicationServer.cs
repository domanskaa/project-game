﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Net;
using ProjectGame.Core.Services;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Exceptions;

namespace ProjectGame.CommunicationServer
{
    public class CommunicationServer
    {
        TcpListener _listener;
        Dictionary<TcpSocketCommunicationClient, ConnectionInfo> _connectionDict;
        MultipleTypesXmlSerializer _serializer;

        private static readonly object _gameNameMut = new object();
        private Dictionary<string, ulong> _gameNameToId;

        private Dictionary<ulong, List<ConnectionInfo>> _gameIdToConnections;
        private static readonly object _gameIdMut = new object();

        private static readonly object _idMutex = new object();
        ulong _lastId = 0;

        private List<GameInfo> _gameInfos = new List<GameInfo>();

        private int _keepAliveInterval;
        public CommunicationServer(int port, int keepAliveInterval)
        {
            _connectionDict = new Dictionary<TcpSocketCommunicationClient, ConnectionInfo>();
            _serializer = new MultipleTypesXmlSerializer();
            _gameNameToId = new Dictionary<string, ulong>();
            _gameIdToConnections = new Dictionary<ulong, List<ConnectionInfo>>();
            _keepAliveInterval = keepAliveInterval;

            IPAddress addr = IPAddress.Parse("0.0.0.0");
            _listener = new TcpListener(addr, port);
            _listener.Start();
        }

        public void Listen()
        {
            while (true) {
                var newClient = _listener.AcceptTcpClient();

                Task.Run(() =>
                {
                    try
                    {
                        HandleClient(newClient);
                    }
                    catch (CommunicationClientException)
                    {
                        Console.WriteLine("Client disconnected due to exception");
                    }
                });
            }
        }

        public void HandleClient(TcpClient cli)
        {
            TcpSocketCommunicationClient comCli = new TcpSocketCommunicationClient(cli, trackKeepAlives:false, timeout:_keepAliveInterval, verbose: false);
            ConnectionInfo info = new ConnectionInfo(comCli);

            while (true)
            {
                try
                {
                    byte[] message = comCli.Receive();
                    var msg = _serializer.Deserialize(message);
                    if (msg == null)
                    {
                        Console.WriteLine("null on deserialize");
                        continue;
                    }
                    handleRequest(info, msg as dynamic);
                } catch(Exception ex)
                {
                    switch(info.Type)
                    {
                        case ConnectionType.GameMaster:
                            {
                                var msg = new GameMasterDisconnected();
                                msg.gameId = info.GameID;
                                var serialized = _serializer.Serialize(msg);
                                lock(_idMutex)
                                {
                                    foreach (ConnectionInfo conn in _gameIdToConnections[info.GameID].FindAll(x => x.ConnectionID != info.ConnectionID))
                                    {
                                        conn.Client.Send(serialized);
                                    }
                                }
                                return;
                            }
                        case ConnectionType.Player:
                            {
                                var msg = new PlayerDisconnected();
                                msg.playerId = info.ConnectionID;
                                var serialized = _serializer.Serialize(msg);
                                lock (_idMutex)
                                {
                                    foreach (ConnectionInfo conn in _gameIdToConnections[info.GameID].FindAll(x => x.ConnectionID != info.ConnectionID))
                                    {
                                        conn.Client.Send(serialized);
                                    }
                                }
                                return;
                            }
                        default: {
                                return;
                            }
                    }
                }
            }
        }

        /// Player requests
        
        public void forwardToGameMaster(ConnectionInfo info, byte[] data)
        {
            ConnectionInfo gm;
            lock(_gameIdMut)
            {
                gm = _gameIdToConnections[info.GameID].Find(x => x.Type == ConnectionType.GameMaster);
            }
            gm.Client.Send(data);
        }

        public void handleRequest(ConnectionInfo info, Discover msg)
        {
            var data = _serializer.Serialize(msg, typeof(Discover));
            forwardToGameMaster(info, data);
        }

        public void handleRequest(ConnectionInfo info, DestroyPiece msg)
        {
            var data = _serializer.Serialize(msg, typeof(DestroyPiece));
            forwardToGameMaster(info, data);
        }

        public void handleRequest(ConnectionInfo info, Move msg)
        {
            var data = _serializer.Serialize(msg, typeof(Move));
            //Console.WriteLine("Move: " + Encoding.UTF8.GetString(data, 0, data.Length));
            forwardToGameMaster(info, data);
        }

        public void handleRequest(ConnectionInfo info, PickUpPiece msg)
        {
            var data = _serializer.Serialize(msg, typeof(PickUpPiece));
            forwardToGameMaster(info, data);
        }

        public void handleRequest(ConnectionInfo info, PlacePiece msg)
        {
            var data = _serializer.Serialize(msg, typeof(PlacePiece));
            forwardToGameMaster(info, data);
        }

        public void handleRequest(ConnectionInfo info, TestPiece msg)
        {
            var data = _serializer.Serialize(msg, typeof(TestPiece));
            forwardToGameMaster(info, data);
        }


        public void handleRequest(ConnectionInfo info, JoinGame jGame)
        {
            lock (_gameNameMut)
            {
                if (_gameNameToId.ContainsKey(jGame.gameName))
                {
                    ulong playerId = getNextId();
                    jGame.playerId = playerId;
                    jGame.playerIdSpecified = true;
                    info.GameID = _gameNameToId[jGame.gameName];
                    info.ConnectionID = playerId;
                    info.Type = ConnectionType.Player;
                    lock (_gameIdMut)
                    {
                        var gamemaster = _gameIdToConnections[info.GameID].Find(x => x.Type == ConnectionType.GameMaster);
                        _gameIdToConnections[info.GameID].Add(info);
                        var msg = _serializer.Serialize(jGame, typeof(JoinGame));
                        gamemaster.Client.Send(msg);
                    }
                }
                else
                {
                    var msg = _serializer.Serialize(new RejectJoiningGame(), typeof(RejectJoiningGame));
                    info.Client.Send(msg);
                }
            }
        }


        public void handleRequest(ConnectionInfo info, GetGames get)
        {
            lock (_gameNameMut)
            {
                RegisteredGames games = new RegisteredGames();
                games.GameInfo = _gameInfos.ToArray();
                var msg = _serializer.Serialize(games, typeof(RegisteredGames));
                info.Client.Send(msg);
            }
        }

        /// GM requests
        
        public void handleRequest(ConnectionInfo info, ConfirmJoiningGame cjg)
        {
            ConnectionInfo target;
            lock(_gameIdMut)
            {
                target = _gameIdToConnections[info.GameID].Find(x => x.ConnectionID == cjg.playerId);
            }
            var msg = _serializer.Serialize(cjg, typeof(ConfirmJoiningGame));
            target.Client.Send(msg);
        }

        public void handleRequest(ConnectionInfo info, RejectJoiningGame rjg)
        {
            ConnectionInfo target;
            lock (_gameIdMut)
            {
                target = _gameIdToConnections[info.GameID].Find(x => x.ConnectionID == rjg.playerId);
                _gameIdToConnections[info.GameID].Remove(target);
            }
            var msg = _serializer.Serialize(rjg, typeof(RejectJoiningGame));
            target.Client.Send(msg);
        }

        public void handleRequest(ConnectionInfo info, Data d)
        {
            lock(_gameIdMut)
            {
                var cons = _gameIdToConnections[info.GameID];
                var msg = _serializer.Serialize(d, typeof(Data));
                
                if(d.playerId == 0) //GameFinished shall be sent to all players
                {
                    foreach(ConnectionInfo conn in cons.FindAll(x => x.Type == ConnectionType.Player))
                    {
                        conn.Client.Send(msg);
                    }
                } else
                {
                    cons.Find(x => x.ConnectionID == d.playerId).Client.Send(msg);
                }
            }
        }

        public void handleRequest(ConnectionInfo info, Game g)
        {
            ConnectionInfo target;
            lock (_gameIdMut)
            {
                var cons = _gameIdToConnections[info.GameID];
                target = cons.Find(x => x.ConnectionID == g.playerId);
            }

            var msg = _serializer.Serialize(g, typeof(Game));
            target.Client.Send(msg);
        }

        public void handleRequest(ConnectionInfo info, RegisterGame reg)
        {
            lock (_gameNameMut)
            {
                if (_gameNameToId.ContainsKey(reg.NewGameInfo.gameName))
                {
                    var msg = _serializer.Serialize(new RejectGameRegistration(), typeof(RejectGameRegistration));
                    info.Client.Send(msg);
                }
                else
                {
                    ulong newId = getNextId();
                    _gameNameToId.Add(reg.NewGameInfo.gameName, newId);
                    _gameInfos.Add(reg.NewGameInfo);
                    
                    info.GameID = newId;
                    info.Type = ConnectionType.GameMaster;

                    var conList = new List<ConnectionInfo>();
                    conList.Add(info);
                    lock (_gameIdToConnections)
                    {
                        _gameIdToConnections[newId] = conList;
                    }

                    var resp = new ConfirmGameRegistration();
                    resp.gameId = newId;
                    var msg = _serializer.Serialize(resp, typeof(ConfirmGameRegistration));
                    info.Client.Send(msg);
                }
            }
        }

        private ulong getNextId()
        {
            lock (_idMutex)
            {
                _lastId++;
                return _lastId;
            }
        }
    }
}
