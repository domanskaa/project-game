﻿using System;
using ProjectGame.Core.Models.Configuration;
using ProjectGame.Core;
using Microsoft.Extensions.CommandLineUtils;
using System.IO;

namespace ProjectGame.CommunicationServer
{
    class Program
    {
        static string XMLPath = "";
        static CommunicationServerSettings communicationServerSettings;
        static int port;

        static void Main(string[] args)
        {
            bool shouldRun = false;
            var app = new CommandLineApplication();
            CommandOption portOption, xmlPathOption;
            InitOptions(app,  out portOption, out xmlPathOption);
            app.OnExecute(() =>
            {
                ParseOptions(app, portOption, xmlPathOption);

                shouldRun = true;
                return 0;
            });

            try
            {
                app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.WriteLine(ex.Message);
                app.ShowHint();
            }

            if (shouldRun)
                RunCommunicationServer();
        }
        private static void InitOptions(CommandLineApplication app, out CommandOption portOption, out CommandOption xmlPathOption)
        {
            app.Name = "Communication Server";
            app.Description = ".NET Core application - Server handling communication between game masters and players";
            var helpOption = app.HelpOption("-h|--help");
            portOption = app.Option("--port <value>",
                    "server port number",
                    CommandOptionType.SingleValue);
            xmlPathOption = app.Option("--conf <value>",
                    "path to XML configuration file",
                    CommandOptionType.SingleValue);
        }

        private static void ParseOptions(CommandLineApplication app,  CommandOption portOption, CommandOption xmlPathOption)
        {
            XMLPath = xmlPathOption.Value();

            if (!int.TryParse(portOption.Value(), out port))
                throw new CommandParsingException(app, "Invalid port number. Aborting");

            if (XMLPath == null)
                throw new CommandParsingException(app, "Invalid path to configuration file. Aborting");

            try
            {
                communicationServerSettings = new XMLConfigurationLoader<CommunicationServerSettings>(XMLPath).GetConfiguration();
            }
            catch (FileNotFoundException)
            {
                throw new CommandParsingException(app, "Configuration file could not be found. Aborting.");
            }
        }

        private static void RunCommunicationServer()
        {

            CommunicationServer cs = new CommunicationServer(port, (int)communicationServerSettings.KeepAliveInterval);
            cs.Listen();
        }
    }
}
