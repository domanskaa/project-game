﻿using ProjectGame.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.CommunicationServer
{
    public class ConnectionInfo
    {
        public ConnectionType Type { get; set; }
        public ulong GameID { get; set; }
        public ulong ConnectionID { get; set; }
        public TcpSocketCommunicationClient Client { get; private set; }

        public ConnectionInfo(TcpSocketCommunicationClient cli)
        {
            Client = cli;
            Type = ConnectionType.Unknown; 
        }
    }
}
