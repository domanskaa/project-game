﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.CommunicationServer
{
    public enum ConnectionType
    {
        Unknown = 0,
        GameMaster = 1,
        Player = 2
    }
}
