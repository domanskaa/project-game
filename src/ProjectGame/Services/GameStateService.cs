﻿using ProjectGame.Core.Models;
using ProjectGame.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectGame.WebAPI.Services
{
    public class GameStateService
    {
        public List<Board[,]> History { get; set; }
        public Board[,] Latest
        {
            get
            {
                if (!History.Any())
                    return null;

                return History.Last();
            }
        }
        public static JsonEvent CreateTestJsonEvent()
        {
            var board = new Board(4, 4, 4);
            foreach (var item in board.Fields)
            {
                item.Type = Core.Enum.FieldType.TaskArea;
                item.Piece = new Piece();
            }

            return CreateJsonEventFromBoard(board);
        }
        public static  JsonEvent CreateJsonEventFromBoard(Board board)
        {
            var jsonEvent = new JsonEvent()
            {
                MapHeight = board.BoardHeight,
                MapWidth = board.BoardWidth,
                Fields = new JsonEvent.FieldDescription[board.BoardWidth, board.BoardHeight]
            };
            for (int x = 0; x < board.BoardWidth; x++)
            {
                for (int y = 0; y < board.BoardHeight; y++)
                {
                    var field = board.Fields[x, y];
                    jsonEvent.Fields[x,y] = CreateField(field);
                }
            }

            return jsonEvent;
        }

        private static JsonEvent.FieldDescription CreateField( Field field)
        {
            var jsonField = new JsonEvent.FieldDescription()
            {
                FieldType = GetFieldLocation(field),
                PieceOnField = field.Piece != null,
                PlayerOnField = GetPlayerType(field),
             };

            if (jsonField.PlayerOnField != JsonEvent.FieldDescription.PlayerType.None)
                jsonField.PlayerHoldsPiece = field.Player.Piece != null;
            if (jsonField.PlayerHoldsPiece)
                jsonField.PlayerHoldsSham = field.Player.Piece.IsSham;
            if (jsonField.PieceOnField)
                jsonField.ShamOnField = field.Piece.IsSham;

            return jsonField;
        }

        public static JsonEvent.FieldDescription.FieldLocation GetFieldLocation(Field field)
        {
            switch (field.Type)
            {
                case Core.Enum.FieldType.TaskArea:
                    return JsonEvent.FieldDescription.FieldLocation.GameArea;
                case Core.Enum.FieldType.GoalCompleted:
                    return JsonEvent.FieldDescription.FieldLocation.GoalAreaDiscovered;
                case Core.Enum.FieldType.GoalNotDiscovered:
                    return JsonEvent.FieldDescription.FieldLocation.GoalAreaUndiscovered;
                case Core.Enum.FieldType.GoalArea:
                case Core.Enum.FieldType.GoalFailed:
                    return JsonEvent.FieldDescription.FieldLocation.GoalArea;
                default:
                    throw new ArgumentException(nameof(field.Type));
            }
        }

        public static JsonEvent.FieldDescription.PlayerType GetPlayerType(Field field)
        {
            if (field.Player == null)
                return JsonEvent.FieldDescription.PlayerType.None;
            if (field.Player.Color == Core.Enum.PlayerColor.Blue)
                return JsonEvent.FieldDescription.PlayerType.Blue;
            return JsonEvent.FieldDescription.PlayerType.Red;
        }

    }
}
