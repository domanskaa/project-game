var socket;
var scheme = document.location.protocol == "https:" ? "wss" : "ws";
var port = document.location.port ? (":" + document.location.port) : "";

connectionUrl = scheme + "://" + document.location.hostname + port + "/ws" ;

socket = new WebSocket(connectionUrl);
table = document.getElementById("board");

function getImageSrc(field) {
    switch(field["PlayerOnField"]){
        case 1: {
            if(field["PlayerHoldsSham"]) {
                return "blue_player_has_sham.jpg"
            }

            if(field["PlayerHoldsPiece"]) {
                return "blue_player_has_piece.jpg"
            }

            if(field["ShamOnField"]) {
                return "blue_player_on_sham.jpg"
            }

            if(field["PieceOnField"]) {
                return "blue_player_on_piece.jpg"
            }
            return "blue_player.jpg"
        }
        case 2: {
            if(field["PlayerHoldsSham"]) {
                return "red_player_has_sham.jpg"
            }

            if(field["PlayerHoldsPiece"]) {
                return "red_player_has_piece.jpg"
            }

            if(field["ShamOnField"]) {
                return "red_player_on_sham.jpg"
            }

            if(field["PieceOnField"]) {
                return "red_player_on_piece.jpg"
            }
            return "red_player.jpg"
        }
        case 3: {
            if(field["PlayerHoldsSham"]) {
                return "blue_leader_has_sham.jpg"
            }

            if(field["PlayerHoldsPiece"]) {
                return "blue_leader_has_piece.jpg"
            }

            if(field["ShamOnField"]) {
                return "blue_leader_on_sham.jpg"
            }

            if(field["PieceOnField"]) {
                return "blue_leader_on_piece.jpg"
            }
            return "blue_leader.jpg"
        }
        case 4: {
            if(field["PlayerHoldsSham"]) {
                return "red_leader_has_sham.jpg"
            }

            if(field["PlayerHoldsPiece"]) {
                return "red_leader_has_piece.jpg"
            }

            if(field["ShamOnField"]) {
                return "red_leader_on_sham.jpg"
            }

            if(field["PieceOnField"]) {
                return "red_leader_on_piece.jpg"
            }
            return "red_leader.jpg"
        }
        default: {
            if(field["ShamOnField"]) {
                return "bad_piece.png"
            }

            if(field["PieceOnField"]) {
                return "piece.png"
            }
            return "empty.png"
        }
    }
}

function getStyle(field) {
    switch(field["FieldType"]) {
        case 0: return "border: 4px solid black;"
        case 1: return "border: 4px solid yellow;"
        case 2: return "border: 4px solid blue;"
        case 3: return "border: 6px solid green;"
    }
}

socket.onmessage = function (e)
{
    newTable = "";
    var obj = JSON.parse(e.data);
    for(i = 0;i<obj["MapHeight"];i++) {
        newTable += "<tr>"
        for(j =0;j<obj["MapWidth"];j++) {
            currentField = obj["Fields"][j][i];
            newTable += "<td style=\"" + getStyle(currentField) +" \"><img src=\"/images/" + getImageSrc(currentField) + "\"></td>"
        }
        newTable += "</tr>"
    }
    table.innerHTML = newTable;
}

/*

Json scheme example in Go

type JsonEvent struct {
	MapWidth  int
	MapHeight int
	Fields    [][]FieldDescription
}

type FieldDescription struct {

	// 0 - None
	// 1 - Blue
	// 2 - Red
	// 3 - BlueLeader
	// 4 - RedLeader
	PlayerOnField int

	PlayerHoldsSham  bool
	PlayerHoldsPiece bool

	//true only if there is a sham on field which is not being held by player
	ShamOnField bool
	//true only if there is a piece on field which is not being held by player
	PieceOnField bool

	// 0 - GameArea
	// 1 - GoalArea
	// 2 - GoalAreaUndiscovered
	// 3 - GoalAreaDiscovered

    FieldType int
}

*/