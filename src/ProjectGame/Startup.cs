﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Net.WebSockets;
using Microsoft.AspNetCore.Http;
using System.Threading;
using ProjectGame.WebAPI.Services;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using ProjectGame.Core.Models;
using System.Diagnostics;

namespace ProjectGame
{
    public class Startup
    {
        private List<WebSocket> _sockets;
        private Board board;
        private string boardJson;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _sockets = new List<WebSocket>();
            board = new Board(4, 4, 4);
            foreach (var item in board.Fields)
            {
                item.Type = Core.Enum.FieldType.TaskArea;
                item.Player = new PlayerMapInfo(1, Core.Enum.PlayerColor.Blue, null);
            }
            Console.WriteLine("TEST JSON");
            Console.WriteLine(JsonConvert.SerializeObject(board));
            Console.WriteLine("END JSON");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseDefaultFiles(new DefaultFilesOptions { DefaultFileNames = new 
                 List<string> { "index.html" } });
            app.UseStaticFiles();

            var webSocketOptions = new WebSocketOptions()
            {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
                ReceiveBufferSize = 4 * 1024
            };
            app.UseWebSockets(webSocketOptions);

            app.Use(async (context, next) =>
            {
                if (context.Request.Path == "/ws")
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        //Board oldBoard = board;
                        string oldBoardJson = boardJson;
                        WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
                        _sockets.Add(webSocket);
                        while (true)
                        {
                            if (oldBoardJson != boardJson)
                            {
                                board = JsonConvert.DeserializeObject<Board>(boardJson);
                                await SendTestJson(context, webSocket);
                                oldBoardJson = boardJson;
                            }
                            await Task.Delay(100);
                        }
                    }
                    else
                    {
                        context.Response.StatusCode = 400;
                    }
                }
                else if (context.Request.Path == "/gm" && HttpMethods.IsPost(context.Request.Method))
                {
                    try
                    {
                        string json;
                        using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8))
                        {
                            json = await reader.ReadToEndAsync();
                        }
                        //board = JsonConvert.DeserializeObject<Board>(json);
                        boardJson = json;
                    }
                    catch (Exception)
                    {
                        context.Response.StatusCode = StatusCodes.Status400BadRequest;
                    }

                }
                else
                {
                    await next();
                }
            });
        }
        private async Task SendTestJson(HttpContext context, WebSocket webSocket)
        {
            var jsonEvent = GameStateService.CreateJsonEventFromBoard(board);
            var json = JsonConvert.SerializeObject(jsonEvent);
            await webSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(json), 0, json.Length), WebSocketMessageType.Text, true, CancellationToken.None);
        }
    }
}
