﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectGame.WebAPI.ViewModels
{
    public class JsonEvent
    {
        public class FieldDescription
        {
            public enum PlayerType
            {
                None = 0,
                Blue = 1,
                Red = 2,
                BlueLeader = 3,
                RedLeader = 4
            }
            public enum FieldLocation
            {
                GameArea = 0,
                GoalArea = 1,
                GoalAreaUndiscovered = 2,
                GoalAreaDiscovered = 3,
            }

            public PlayerType PlayerOnField { get; set; }
            public FieldLocation FieldType { get; set; }
            public bool PlayerHoldsSham { get; set; }
            public bool PlayerHoldsPiece { get; set; }
            public bool ShamOnField { get; set; }
            public bool PieceOnField { get; set; }

        }
        public uint MapWidth { get; set; }
        public uint MapHeight { get; set; }
        public FieldDescription[,] Fields { get; set; }

    }
}
