﻿
using ProjectGame.Core.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.Core.Models.Messages;

namespace ProjectGame.Core.Converters
{
    public static class FieldTypeConverter
    {
        public static FieldType ToFieldType(this GoalFieldType goalFieldType)
        {
            switch(goalFieldType)
            {
                case GoalFieldType.goal:
                    return FieldType.GoalCompleted;
                case GoalFieldType.nongoal:
                    return FieldType.GoalFailed;
                case GoalFieldType.unknown:
                    return FieldType.GoalArea;
                default:
                    throw new ArgumentException(nameof(goalFieldType));
            }
        }
    }
}
