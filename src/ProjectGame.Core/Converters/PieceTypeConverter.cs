﻿using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Converters
{
    public static class PieceTypeConverter
    {
        public static bool ToIsTested(this PieceType type)
        {
            return type != PieceType.unknown;
        }

        public static bool ToIsSham(this PieceType type)
        {
            return type == PieceType.sham;
        }
    }
}
