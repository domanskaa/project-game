﻿using ProjectGame.Core.Enum;
using ProjectGame.Core.Models.Configuration;
using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Converters
{
    public static class ColorConverter
    {
        public static PlayerColor ToPlayerColour(this MessageTeamColour color)
        {
            return color == MessageTeamColour.red ? PlayerColor.Red : PlayerColor.Blue;
        }
        public static MessageTeamColour ToMessageTeamColour(this PlayerColor color)
        {
            return color == PlayerColor.Red ? MessageTeamColour.red : MessageTeamColour.blue;
        }

        public static TeamOwnership ToTeamOwnership(this MessageTeamColour color)
        {
            return color == MessageTeamColour.red ? TeamOwnership.Red : TeamOwnership.Blue;
        }
    }
}
