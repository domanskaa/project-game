using System;
using System.Collections;
using ProjectGame.CommunicationServer.Interfaces;
using ProjectGame.Core.Models.Messages;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Xml;

namespace ProjectGame.Core.Services
{
    public class MultipleTypesXmlSerializer: ISerializer
    {
        Hashtable _xmlSerializers;
        XmlSerializer[] _xmlDeserializers;

        public MultipleTypesXmlSerializer()
        {
            _xmlSerializers = CreateXmlSerializers();
            _xmlDeserializers = CreateXmlDeserializers();
        }

        public byte[] Serialize<T>(T obj) => Serialize(obj, typeof(T));
        
        public byte[] Serialize(object obj, Type objType)
        {
            XmlSerializer xs = (XmlSerializer)_xmlSerializers[objType];
            using(MemoryStream ms = new MemoryStream())
            {
                xs.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public object Deserialize(byte[] bytes)
        {
            using(MemoryStream ms = new MemoryStream(bytes))
            {
                XmlReader xmlReader = XmlReader.Create(ms);
                foreach(var xds in _xmlDeserializers)
                {
                    if(xds.CanDeserialize(xmlReader))
                        return xds.Deserialize(xmlReader);
                }
            }

            return null;
        }
        
        private Hashtable CreateXmlSerializers()
        {
            Hashtable xmlSerializers = new Hashtable();
            Type[] types = {typeof(ConfirmJoiningGame),typeof(RejectJoiningGame), typeof(Game), typeof(GameStarted),  typeof(RegisterGame),
                            typeof(Move), typeof(Discover), typeof(PickUpPiece), typeof(PlacePiece), typeof(RejectKnowledgeExchange),
                            typeof(DestroyPiece), typeof(TestPiece), typeof(AuthorizeKnowledgeExchange), typeof(JoinGame),
                            typeof(ConfirmGameRegistration), typeof(KnowledgeExchangeRequest), typeof(Data),
                            typeof(PlayerDisconnected), typeof(GameMasterDisconnected)};

            foreach(Type type in types)
            {
                XmlSerializer serializer = new XmlSerializer(type);
                xmlSerializers.Add(type, serializer);
            }

            return xmlSerializers;
        }

        private XmlSerializer[] CreateXmlDeserializers()
        {
            Type[] types = {typeof(ConfirmJoiningGame),typeof(RejectJoiningGame), typeof(Game), typeof(GameStarted),  typeof(RegisterGame),
                            typeof(Move), typeof(Discover), typeof(PickUpPiece), typeof(PlacePiece), typeof(RejectKnowledgeExchange),
                            typeof(DestroyPiece), typeof(TestPiece), typeof(AuthorizeKnowledgeExchange), typeof(JoinGame),
                            typeof(ConfirmGameRegistration), typeof(KnowledgeExchangeRequest), typeof(Data),
                            typeof(PlayerDisconnected), typeof(GameMasterDisconnected)};

            XmlSerializer[] xmlDeserializers = new XmlSerializer[types.Length];

            for(int i = 0; i < types.Length; i++)
                xmlDeserializers[i] = new XmlSerializer(types[i]);

            return xmlDeserializers;
        }
    }
}