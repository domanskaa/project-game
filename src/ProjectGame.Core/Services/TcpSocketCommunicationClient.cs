using System;
using System.Collections.Generic;
using ProjectGame.CommunicationServer.Interfaces;
using System.Net.Sockets;
using System.Threading.Tasks;
using ProjectGame.Core.Helpers;
using System.Text;
using System.Threading;
using ProjectGame.Core.Exceptions;

namespace ProjectGame.Core.Services
{
    public class TcpSocketCommunicationClient: ICommunicationClient
    {
        NetworkStream _stream;
        BlockingQueue _messagesQueue;

        const byte ETB = 23; //End transmission blocks
        const int MAX_CHUNK_SIZE = 0x10000;

        private readonly object _mut = new object();

        private bool _sendKeepAlives;
        private bool _trackKeepAlives;

        private bool _alive;
        private int _keepAliveTimeout;
        private bool _verbose;

        public TcpSocketCommunicationClient(TcpClient client, bool sendKeepAlives = false, bool trackKeepAlives = false, int timeout = 0, bool verbose = false)
        {
            _stream = client.GetStream();
            _messagesQueue = new BlockingQueue();
            _sendKeepAlives = sendKeepAlives;
            _trackKeepAlives = trackKeepAlives;
            _alive = true;
            _keepAliveTimeout = timeout;
            _verbose = verbose;

            Thread receiveAndEnqueue = new Thread(() =>
            {
                try
                {
                    ReceiveAndEnqueue();
                }
                catch (CommunicationClientException)
                {
                    _messagesQueue.Enqueue(null);
                }
            });
            receiveAndEnqueue.Start();

            if (_trackKeepAlives)
            {
                _stream.ReadTimeout = timeout;
            }

            if (_sendKeepAlives)
            {
               
                Thread keepAliveSending = new Thread(() =>
                {
                    try
                    {
                        while (_alive)
                        {
                            Thread.Sleep(timeout/2);
                            Send(new byte[] { });
                        }
                    }
                    catch (CommunicationClientException)
                    {
                        _messagesQueue.Enqueue(null);
                    }
                }); 
            }
        }

        public void Send(byte[] bytes)
        {
            lock (_mut)
            {
                byte[] data = new byte[bytes.Length + 1];

                bytes.CopyTo(data, 0);
                data[bytes.Length] = ETB;

                try
                {
                    _stream.Write(data, 0, data.Length);
                }
                catch (System.IO.IOException)
                {
                  
                }
            }
        }

        public byte[] Receive()
        {
            var resp = (byte[])_messagesQueue.Dequeue();
            if(resp == null)
            {
                throw new CommunicationClientException();
            }
            return resp;
        }
        private void ReceiveAndEnqueue()
        {
            byte[] messagePart = new byte[MAX_CHUNK_SIZE];
            int lengthOfMessagePart = 0;

            while(true)
            {
                byte[] buf = new byte[MAX_CHUNK_SIZE];
                int bytesRead = 0;
                try
                {
                    bytesRead = _stream.Read(buf, 0, MAX_CHUNK_SIZE);
                }
                catch (System.IO.IOException)
                {
                    throw new CommunicationClientException();
                }

                if (bytesRead == 0)
                {
                    if(_trackKeepAlives)
                    {
                        _alive = false;
                        _messagesQueue.Enqueue(null);
                        break;
                    } else
                    {
                        Console.WriteLine("not predicted behviour - bytes read 0 without keepalive");
                    }
                }

                var endIndex = Array.IndexOf(buf, ETB);
                var beginIndex = 0;
                while(endIndex != -1)
                {

                    byte[] msg = new byte[endIndex - beginIndex + lengthOfMessagePart];

                    Array.Copy(messagePart, 0, msg, 0, lengthOfMessagePart);
                    Array.Copy(buf, beginIndex, msg, lengthOfMessagePart, endIndex - beginIndex);
                    if(_verbose)
                        Console.WriteLine("M: " + Encoding.UTF8.GetString(msg, 0, endIndex - beginIndex + lengthOfMessagePart));

                    if (msg.Length == 0)
                    {
                        if (_trackKeepAlives)
                        {
                            Send(new byte[] { });
                        }
                    }
                    else
                    {
                        _messagesQueue.Enqueue(msg);
                    }

                    beginIndex = endIndex + 1;
                    endIndex = Array.IndexOf(buf, ETB, beginIndex);
                    lengthOfMessagePart = 0;
                }

                int tempMessagePartLength = bytesRead - beginIndex;
                Array.Copy(buf, beginIndex, messagePart, lengthOfMessagePart, tempMessagePartLength);
                lengthOfMessagePart += tempMessagePartLength;
                
                //Console.WriteLine("MP: " + Encoding.UTF8.GetString(messagePart, 0, lengthOfMessagePart));
            }

        }
        
    }
}