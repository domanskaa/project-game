﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Models
{
    public class Piece : ICloneable
    {
        public ulong Id { get; set; }
        public ulong? PlayerId { get; set; } = null;
        public bool IsSham { get; set; }
        public bool IsTested { get; set; }

        public Piece() { }
        public Piece(ulong id, bool isSham, bool isTested)
        {
            this.Id = id;
            this.IsSham = isSham;
            this.IsTested = isTested;
        }

        public object Clone()
        {
            return new Piece(Id, IsSham, IsTested);
        }
    }
}
