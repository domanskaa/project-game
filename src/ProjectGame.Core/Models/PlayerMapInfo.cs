﻿using ProjectGame.Core.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Models
{
    public class PlayerMapInfo : ICloneable
    {
        public ulong Id { get; set; }
        public PlayerColor Color { get; set; }
        public Piece Piece { get; set; }
        public PlayerMapInfo()
        {
        }

        public PlayerMapInfo(ulong id, PlayerColor color, Piece piece)
        {
            Id = id;
            Color = color;
            Piece = piece;
        }

        public object Clone()
        {
            return new PlayerMapInfo(Id, Color, Piece?.Clone() as Piece);
        }
    }
}
