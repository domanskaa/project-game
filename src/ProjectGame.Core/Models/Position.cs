﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Models
{
    public class Position : ICloneable
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }
        public override bool Equals(object obj)
        {
            var pos = (obj as Position);
            if (obj is Position)
                return pos.X == X && pos.Y == Y;
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public object Clone()
        {
            return new Position(X, Y);
        }

    }
}
