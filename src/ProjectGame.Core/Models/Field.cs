﻿using ProjectGame.Core.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Models
{
    public class Field : ICloneable
    {
        public FieldType Type { get; set; }
        public TeamOwnership TeamOwnership { get; set; }
        public PlayerMapInfo Player { get; set; }
        public Piece Piece { get; set; }
        public DateTimeOffset LastSeen { get; set; }
        public Position Position { get; set; }
        public int? ManhattanDistance { get; set; } = null;
        public bool IsOccupied => Player != null;
        public bool InGoalArea => (Type == FieldType.GoalArea) || (Type == FieldType.GoalCompleted) || (Type == FieldType.GoalFailed) || (Type == FieldType.GoalNotDiscovered);        

        public Field(int x, int y)
        {
            Position = new Position(x, y);
        }

        public object Clone()
        {
            return new Field(Position.X, Position.Y)
            {
                Type = Type,
                TeamOwnership = TeamOwnership,
                Player = Player?.Clone() as PlayerMapInfo,
                LastSeen = LastSeen,
                Position = Position?.Clone() as Position,
                ManhattanDistance = ManhattanDistance,
            };
        }
    }
}
