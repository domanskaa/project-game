﻿using ProjectGame.Core.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Models
{
    public class Board : ICloneable
    {
        public Field[,] Fields { get; set; }
        public uint BoardHeight;
        public uint BoardWidth;
        public uint GoalAreaHeight;
        public uint TaskAreaHeight;

        public Board(uint goalAreaHeight, uint boardWidth, uint taskAreaHeight)
        {
            BoardWidth = boardWidth;
            TaskAreaHeight = taskAreaHeight;
            GoalAreaHeight = goalAreaHeight;

            BoardHeight = 2 * GoalAreaHeight + TaskAreaHeight;
            Fields = new Field[BoardWidth, BoardHeight];

            for(int i=0; i < BoardWidth; i++)
            {
                for(int j=0; j < BoardHeight; j++)
                {
                    Fields[i, j] = new Field(i, j);
                }
            }
        }

        public Field GetField(Position pos)
        {
            return Fields[pos.X, pos.Y];
        }

        public object Clone()
        {
            return new Board(GoalAreaHeight, BoardWidth, TaskAreaHeight)
            {
                Fields = Fields?.Clone() as Field[,],
            };
        }

        public void InitializeFields()
        {
            for (uint i = 0; i < BoardWidth; i++)
            {
                for (uint j = 0; j < BoardHeight; j++)
                {
                    Fields[i, j].TeamOwnership = TeamOwnership.None;
                    if (j < GoalAreaHeight)
                    {
                        Fields[i, j].Type = FieldType.GoalArea;
                        Fields[i, j].TeamOwnership = TeamOwnership.Blue;
                    }
                    else if (j >= GoalAreaHeight + TaskAreaHeight)
                    {
                        Fields[i, j].Type = FieldType.GoalArea;
                        Fields[i, j].TeamOwnership = TeamOwnership.Red;
                    }
                    else
                    {
                       Fields[i, j].Type = FieldType.TaskArea;
                    }
                }
            }
        }
    }


}
