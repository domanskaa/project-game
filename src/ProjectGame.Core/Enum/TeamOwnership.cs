﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Enum
{
    public enum TeamOwnership
    {
        None,
        Blue,
        Red
    }
}
