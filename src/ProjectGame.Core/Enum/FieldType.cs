﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.Core.Enum
{
    public enum FieldType
    {
        TaskArea,
        GoalArea,
        GoalCompleted,
        GoalFailed,

        // Game master specific
        GoalNotDiscovered
    }
}
