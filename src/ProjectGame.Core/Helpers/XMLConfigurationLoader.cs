
using ProjectGame.Core.Models.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using ProjectGame.Core.Interfaces;

namespace ProjectGame.Core
{
    public class XMLConfigurationLoader<T> : IConfigurationLoader<T> where T : Configuration
    {
        private T _configuration;
        public XMLConfigurationLoader(string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (Stream reader = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                _configuration = (T)serializer.Deserialize(reader);
            }
        }

        public T GetConfiguration()
        {
            return _configuration;
        }
    }
}
