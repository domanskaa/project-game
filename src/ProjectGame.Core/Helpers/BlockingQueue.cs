﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ProjectGame.Core.Helpers
{
    public class BlockingQueue
    {
        private Queue<object> _queue = new Queue<object>();
        private SemaphoreSlim semaphore = new SemaphoreSlim(0);
        public void Enqueue(object obj)
        {
            lock (_queue)
            {
                _queue.Enqueue(obj);
                semaphore.Release();
            }
        }
        public object Dequeue()
        {
            semaphore.Wait();
            lock (_queue)
            {
                return _queue.Dequeue();
            }
        }
    }
}
