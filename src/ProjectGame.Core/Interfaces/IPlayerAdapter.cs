﻿using ProjectGame.Core.Models.Messages;

namespace ProjectGame.Core.Interfaces
{
    public interface IPlayerAdapter
    {
        object Receive();

        void MakeGetGamesReuqest(GetGames getGames);
        void MakeJoinGameRequest(JoinGame joinGame);
        
        // Player in-game actions
        void MakeTestRequest(TestPiece testResponseData);
        void MakeMoveRequest(Move moveResponseData);
        void MakePickUpPieceRequest(PickUpPiece pickUpPieceData);
        void MakePlacePieceRequest(PlacePiece placePieceData);
        void MakeDestroyPieceRequest(DestroyPiece destroyPieceData);
        void MakeDiscoverRequest(Discover discoverData);

        // Player knowledge exchange
        //void MakeAcceptRejectKnowledgeExchangeRequest(AcceptExchangeRequest acceptKnowledgeExchange);
        //void MakeRejectKnowledgeExchangeRequest(RejectKnowledgeExchange rejectKnowledgeExchange);
        //void MakeKnowledgeExchangeRequest(KnowledgeExchangeRequest knowledgeRequest);
    }
}
