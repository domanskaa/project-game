﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.CommunicationServer.Interfaces
{
    public interface ISerializer
    {
        byte[] Serialize<T>(T obj);
        byte[] Serialize(object obj, Type objType);
        object Deserialize(byte[] bytes);
    }
}
