﻿using ProjectGame.Core.Models.Messages;

namespace ProjectGame.Core.Interfaces
{
    public interface IGameMasterAdapter
    {
        object Receive();

        void MakeRegisterGameRequest(RegisterGame registerGame);

        // Player game control
        void MakeConfirmJoiningGameRequest(ConfirmJoiningGame confirmJoiningGame);
        void MakeRejectJoiningGameRequest(RejectJoiningGame rejectJoiningGame);
        void MakeGameRequest(Game game);

        // Player in-game actions
        void MakeTestReponseRequest(Data testResponseData);
        void MakeMoveResponseRequest(Data moveResponseData);
        void MakePickUpResponseRequest(Data pickUpResponseData);
        void MakePlaceResponseRequest(Data placeResponseData);
        void MakeDestroyResponseRequest(Data destroyResponseData);
        void MakeDiscoverResponseRequest(Data discoverResponseData);
        void MakeGameFinishedRequest(Data gameFinished);
        // Player knowledge exchange
        //void MakeAuthorizeKnowledgeExchangeRequest(AuthorizeKnowledgeExchange authorizeKnowledgeExchange);
        //void MakeRejectKnowledgeExchangeRequest(RejectKnowledgeExchange rejectKnowledgeExchange);
    }
}
