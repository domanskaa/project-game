using ProjectGame.Core.Models.Configuration;

namespace ProjectGame.Core.Interfaces
{
    public interface IConfigurationLoader<T> where T : Configuration
    {
        T GetConfiguration();
    }
}
