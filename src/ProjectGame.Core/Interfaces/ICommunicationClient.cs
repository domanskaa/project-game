﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.CommunicationServer.Interfaces
{
    public interface ICommunicationClient
    {
        void Send(byte[] bytes);
        byte[] Receive();
    }
}
