﻿using Moq;
using ProjectGame.Core.Converters;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Models;
using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ProjectGame.UnitTests.PlayerTests.ConnectToGameTests
{
    public class ConnectToGameTests : PlayerTestsBase
    {
        private JoinGame joinGame;
        public ConnectToGameTests()
        {
            mockAdapter.Setup(a => a.MakeJoinGameRequest(It.IsAny<JoinGame>())).Callback((JoinGame jg) => joinGame = jg);
        }

        [Fact]
        public void RejectJoiningGameTest()
        {
            // Arrange
            var request = new JoinGame();
            responseStack.Push(new RejectJoiningGame());

            // Act & Assert
            Assert.Throws<InvalidOperationException>(() => player.ConnectToGame(request));
            Assert.Equal(joinGame, request);
        }

        [Fact]
        public void GameNotStartedTest()
        {
            // Arrange
            var request = new JoinGame();
            responseStack.Push(new ConfirmJoiningGame());
            responseStack.Push(new RejectJoiningGame());

            // Act & Assert
            Assert.Throws<InvalidOperationException>(() => player.ConnectToGame(request));
            Assert.Equal(joinGame, request);
        }

        [Theory]
        [InlineData(PlayerType.member, PlayerColor.Blue)]
        [InlineData(PlayerType.member, PlayerColor.Red)]
        [InlineData(PlayerType.leader, PlayerColor.Blue)]
        [InlineData(PlayerType.leader, PlayerColor.Red)]
        public void ProperProcessingResponsesTest(PlayerType playerType, PlayerColor playerColor)
        {
            var request = new JoinGame();
            var confirmJoiningResponse = new ConfirmJoiningGame()
            {
                gameId = gameId,
                playerId = playerId,
                PlayerDefinition = new PlayerDto()
                {
                    id = playerId,
                    team = playerColor == PlayerColor.Red ? MessageTeamColour.red : MessageTeamColour.blue,
                    type = playerType
                },
                privateGuid = guid
            };
            var expectedLocation = new Position(1, 1);
            var tmpId = playerId;
            var players = new List<PlayerMapInfo>
            {
                new PlayerMapInfo(){Color = playerColor, Id = playerId},
                new PlayerMapInfo(){Color = PlayerColor.Blue, Id = ++tmpId},
                new PlayerMapInfo(){Color = PlayerColor.Red, Id = ++tmpId},
                new PlayerMapInfo(){Color = PlayerColor.Blue, Id = ++tmpId},
                new PlayerMapInfo(){Color = PlayerColor.Red, Id = ++tmpId},
            };
            uint width = 3;
            uint taskLength = 3;
            uint goalLength = 3;
            var startGameResponse = new Game()
            {
                Board = new GameBoard() { width = width, goalsHeight = goalLength, tasksHeight = taskLength },
                playerId = playerId,
                PlayerLocation = new LocationDto() { x = (uint)expectedLocation.X, y = (uint)expectedLocation.Y },
                Players = players.Select(p => new PlayerDto() { id = p.Id, team = p.Color.ToMessageTeamColour(), type = PlayerType.member }).ToArray(),
            };
            responseStack.Push(startGameResponse);
            responseStack.Push(confirmJoiningResponse);

            // Act
            player.ConnectToGame(request);

            Assert.Equal(playerColor, playerGameState.Color);
            Assert.Null(playerGameState.Piece);
            Assert.Equal(expectedLocation.X, playerGameState.Position.X);
            Assert.Equal(expectedLocation.Y, playerGameState.Position.Y);
            AssertBoard(expectedLocation, width, taskLength, goalLength);
        }

        private void AssertBoard(Position expectedLocation, uint width, uint taskLength, uint goalLength)
        {
            Assert.Equal(goalLength, playerGameState.Board.GoalAreaHeight);
            Assert.Equal(taskLength, playerGameState.Board.TaskAreaHeight);
            Assert.Equal(width, playerGameState.Board.BoardWidth);
            Assert.Equal(2 * goalLength + taskLength, playerGameState.Board.BoardHeight);
            Assert.Equal(playerId, playerGameState.Board.Fields[expectedLocation.X, expectedLocation.Y].Player.Id);
        }
    }
}
