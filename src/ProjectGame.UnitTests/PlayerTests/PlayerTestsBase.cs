﻿using Moq;
using ProjectGame.Core.Interfaces;
using ProjectGame.Core.Models.Messages;
using ProjectGame.PlayerProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.UnitTests.PlayerTests
{
    public class PlayerTestsBase
    {
        protected PlayerGameState playerGameState;
        protected Mock<IPlayerAdapter> mockAdapter;
        protected ulong gameId = 1;
        protected ulong playerId = 2;
        protected string guid = "tst-guid";
        protected Stack<PlayerMessage> responseStack;
        protected Player player;

        public PlayerTestsBase()
        {
            mockAdapter = new Mock<IPlayerAdapter>();
            mockAdapter.Setup(a => a.Receive()).Returns(() => responseStack.Pop());
            playerGameState = new PlayerGameState();
            player = new Player(mockAdapter.Object, new SimpleStrategy(), playerGameState);
            responseStack = new Stack<PlayerMessage>();
        }
    }
}
