﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using ProjectGame.Core.Models.Messages;

namespace ProjectGame.UnitTests.PlayerTests.MakeActionTests
{
    public class TestPieceTests : MakeActionTestsBase
    {
        private TestPiece testPieceRequest;

        public TestPieceTests()
        {
            mockAdapter.Setup(a => a.MakeTestRequest(It.IsAny<TestPiece>())).Callback((TestPiece req) => testPieceRequest = req);
        }

        [Fact]
        public void NoPieceTest()
        {
            // Arrange
            playerGameState.Piece = null;
            var testPiece = new TestPiece();

            // Act & Assert
            Assert.Throws<InvalidOperationException>(() => player.MakeAction(testPiece));
        }

        [Fact]
        public void CarryingPieceTest()
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            var expectedData = new Data()
            {
                Pieces = new PieceDto[] { new PieceDto() { id = 1, type = PieceType.sham} },
            };
            responseStack.Push(expectedData);
            playerGameState.Piece = new Core.Models.Piece() { Id = 1, IsSham = false, IsTested = false};

            // Act
            var data = player.MakeAction(new TestPiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, testPieceRequest);
            Assert.True(playerGameState.Piece.IsSham);
            Assert.True(playerGameState.Piece.IsTested);
        }
    }
}
