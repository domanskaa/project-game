﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Models;

namespace ProjectGame.UnitTests.PlayerTests.MakeActionTests
{
    public class MoveTests : MakeActionTestsBase
    {
        protected Move placePieceRequest;

        public MoveTests()
        {
            mockAdapter.Setup(a => a.MakeMoveRequest(It.IsAny<Move>())).Callback((Move req) => placePieceRequest = req);
        }

        [Theory]
        [InlineData(MoveType.up)]
        [InlineData(MoveType.down)]
        [InlineData(MoveType.left)]
        [InlineData(MoveType.right)]
        public void CorrectMoveTest(MoveType moveType)
        {
            // Arrange
            Position newPosition = CalculatePosition(moveType);
            FakeConnectToGame(gameId, guid);
            int manhatanDistance = 5;
            var expectedData = new Data()
            {
                TaskFields = new TaskFieldDto[] { new TaskFieldDto() { x = (uint)newPosition.X, y = (uint)newPosition.Y, timestamp = newDateTime, distanceToPiece = manhatanDistance } },
                PlayerLocation = new LocationDto() { x = (uint)newPosition.X, y = (uint)newPosition.Y}
            };
            responseStack.Push(expectedData);
            playerGameState.Position = oldPosition;
            var field = playerGameState.Board.Fields[newPosition.X, newPosition.Y];
            field.ManhattanDistance = manhatanDistance - 1;
            field.LastSeen = oldDateTime;

            // Act
            var data = player.MakeAction(new Move());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.Equal(newPosition.X, playerGameState.Position.X);
            Assert.Equal(newPosition.Y, playerGameState.Position.Y);
            Assert.Equal(newDateTime, field.LastSeen);
            Assert.Equal(manhatanDistance, field.ManhattanDistance);
        }

        [Fact]
        public void OutOfBoardMoveTest()
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            var expectedData = new Data()
            {
                TaskFields = new TaskFieldDto[] { },
                PlayerLocation = new LocationDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y }
            };
            responseStack.Push(expectedData);
            playerGameState.Position = oldPosition;
            // Act
            var data = player.MakeAction(new Move());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.Equal(oldPosition.X, playerGameState.Position.X);
            Assert.Equal(oldPosition.Y, playerGameState.Position.Y);
        }

        [Fact]
        public void PositionOccupiedByOtherPlayerMoveTest()
        {
            // Arrange
            Position newPosition = new Position(oldPosition.X + 1, oldPosition.Y);
            FakeConnectToGame(gameId, guid);
            int manhatanDistance = 1;
            ulong playerId = 2;
            var expectedData = new Data()
            {
                TaskFields = new TaskFieldDto[] { new TaskFieldDto() { x = (uint)newPosition.X, y = (uint)newPosition.Y,
                    timestamp = newDateTime, distanceToPiece = manhatanDistance, playerIdSpecified = true, pieceId = playerId } },
                Pieces = new PieceDto[] { new PieceDto() { timestamp = newDateTime, playerIdSpecified = true,
                    playerId = playerId, type = PieceType.unknown} },
                PlayerLocation = new LocationDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y }
            };
            responseStack.Push(expectedData);
            playerGameState.Position = oldPosition;
            var field = playerGameState.Board.Fields[newPosition.X, newPosition.Y];
            field.ManhattanDistance = manhatanDistance - 1;
            field.LastSeen = oldDateTime;

            // Act
            var data = player.MakeAction(new Move());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.Equal(oldPosition.X, playerGameState.Position.X);
            Assert.Equal(oldPosition.Y, playerGameState.Position.Y);
            Assert.Equal(newDateTime, field.LastSeen);
            Assert.Equal(manhatanDistance, field.ManhattanDistance);
        }

        private Position CalculatePosition(MoveType moveType)
        {
            var newPosition = new Position(oldPosition.X, oldPosition.Y);
            switch (moveType)
            {
                case MoveType.down:
                    newPosition.Y--;
                    break;
                case MoveType.up:
                    newPosition.Y++;
                    break;
                case MoveType.left:
                    newPosition.X--;
                    break;
                case MoveType.right:
                    newPosition.X++;
                    break;
            }

            return newPosition;
        }
    }
}
