﻿using Moq;
using ProjectGame.Core.Interfaces;
using ProjectGame.Core.Models;
using ProjectGame.Core.Models.Messages;
using ProjectGame.PlayerProject;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ProjectGame.UnitTests.PlayerTests.MakeActionTests
{
    public class MakeActionTestsBase : PlayerTestsBase
    {
        protected DateTime oldDateTime = new DateTime(2018, 1, 1, 1, 1, 1);
        protected DateTime newDateTime = new DateTime(2019, 2, 2, 2, 2, 2);
        protected Position oldPosition = new Position(1, 4);
        public MakeActionTestsBase()
        {
            mockAdapter.Setup(a => a.MakeJoinGameRequest(It.IsAny<JoinGame>())).Callback(() => { });
        }

        protected void AssertCredentialsSet(ulong gameId, string guid, Data expectedData, Data data, GameMessage message)
        {
            Assert.Equal(expectedData, data);
            Assert.Equal(message.gameId, gameId);
            Assert.Equal(message.playerGuid, guid);
        }

        protected void FakeConnectToGame(ulong gameId, string guid)
        {
            responseStack.Push(new Game()
            {
                PlayerLocation = new LocationDto(),
                playerId = 1,
                Board = new GameBoard() { goalsHeight = 3, tasksHeight = 3, width = 3 },
                Players = new PlayerDto[0]
            });
            responseStack.Push(new ConfirmJoiningGame()
            {
                gameId = gameId,
                playerId = 1,
                privateGuid = guid,
                PlayerDefinition = new PlayerDto() { id = 1, team = MessageTeamColour.blue, type = PlayerType.member }
            });
            player.ConnectToGame(new JoinGame());
        }
    }
}
