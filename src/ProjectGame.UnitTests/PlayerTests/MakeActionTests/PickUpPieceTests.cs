﻿using Moq;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ProjectGame.UnitTests.PlayerTests.MakeActionTests
{
    public class PickUpPieceTests : MakeActionTestsBase
    {
        protected PickUpPiece pickUpPieceRequest;
        public PickUpPieceTests()
        {
            mockAdapter.Setup(a => a.MakePickUpPieceRequest(It.IsAny<PickUpPiece>())).Callback((PickUpPiece req) => pickUpPieceRequest = req);
        }

        [Fact]
        public void NoPieceToPickUpTest()
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            var expectedData = new Data()
            {
                Pieces = null,
            };
            responseStack.Push(expectedData);
            playerGameState.Piece = null;

            // Act
            var data = player.MakeAction(new PickUpPiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, pickUpPieceRequest);
            Assert.Null(playerGameState.Piece);
        }

        [Fact]
        public void PickUpTest()
        {
            // Arrange
            ulong expectedPieceId = 1;
            FakeConnectToGame(gameId, guid);
            var expectedData = new Data()
            {
                Pieces = new PieceDto[] { new PieceDto() { id = expectedPieceId, type = PieceType.unknown} },
            };
            responseStack.Push(expectedData);
            playerGameState.Piece = null;

            // Act
            var data = player.MakeAction(new PickUpPiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, pickUpPieceRequest);
            Assert.NotNull(playerGameState.Piece);
            Assert.Equal(expectedPieceId, playerGameState.Piece.Id);
            Assert.False(playerGameState.Piece.IsSham);
            Assert.False(playerGameState.Piece.IsTested);
        }

        [Fact]
        public void PlayerCarryingPieceTest()
        {
            // Arrange
            ulong expectedPieceId = 2;
            FakeConnectToGame(gameId, guid);
            var expectedData = new Data()
            {
                Pieces = new PieceDto[] { new PieceDto() { id = expectedPieceId - 1} },
            };
            responseStack.Push(expectedData);
            playerGameState.Piece = new Core.Models.Piece() { Id = expectedPieceId };

            // Act
            var data = player.MakeAction(new PickUpPiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, pickUpPieceRequest);
            Assert.NotNull(playerGameState.Piece);
            Assert.Equal(expectedPieceId, playerGameState.Piece.Id);
        }

        [Fact]
        public void PlayerCarryingPieceNoPieceToPickUpTest()
        {
            // Arrange
            ulong expectedPieceId = 2;
            FakeConnectToGame(gameId, guid);
            var expectedData = new Data()
            {
                Pieces = new PieceDto[] { },
            };
            responseStack.Push(expectedData);
            playerGameState.Piece = new Core.Models.Piece() { Id = expectedPieceId };

            // Act
            var data = player.MakeAction(new PickUpPiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, pickUpPieceRequest);
            Assert.NotNull(playerGameState.Piece);
            Assert.Equal(expectedPieceId, playerGameState.Piece.Id);
        }
    }

}
