﻿using Moq;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Interfaces;
using ProjectGame.Core.Models.Messages;
using ProjectGame.PlayerProject;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ProjectGame.UnitTests.PlayerTests.MakeActionTests
{
    public class PlacePieceTests : MakeActionTestsBase
    {
        protected PlacePiece placePieceRequest;

        public PlacePieceTests()
        {
            mockAdapter.Setup(a => a.MakePlacePieceRequest(It.IsAny<PlacePiece>())).Callback((PlacePiece req) => placePieceRequest = req);
        }

        [Fact]
        public void NoPieceTest()
        {
            // Arrange
            playerGameState.Piece = null;
            var placePiece = new PlacePiece();

            // Act & Assert
            Assert.Throws<InvalidOperationException>(() => player.MakeAction(placePiece));
        }

        [Fact]
        public void RefusePlacingPieceTest()
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            var expectedData = new Data()
            {
                Pieces = new PieceDto[] { new PieceDto() },
            };
            responseStack.Push(expectedData);
            playerGameState.Piece = new Core.Models.Piece();

            // Act
            var data = player.MakeAction(new PlacePiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.NotNull(playerGameState.Piece);
        }


        [Theory]
        [InlineData(GoalFieldType.goal)]
        [InlineData(GoalFieldType.nongoal)]
        public void GoalAreaNonShamTest(GoalFieldType goalFieldType)
        {
            // Arrange
            var expectedFieldType = goalFieldType == GoalFieldType.goal ? FieldType.GoalCompleted : FieldType.GoalFailed;
            FakeConnectToGame(gameId, guid);
            Data expectedData = CreateGoalAreaResponse(newDateTime, goalFieldType);
            responseStack.Push(expectedData);
            playerGameState.Piece = new Core.Models.Piece() { IsSham = false, PlayerId = playerId };
            var field = playerGameState.Board.Fields[playerGameState.Position.X, playerGameState.Position.Y];
            field.LastSeen = oldDateTime;

            // Act
            var data = player.MakeAction(new PlacePiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.Null(playerGameState.Piece);
            Assert.Equal(expectedFieldType, field.Type);
            Assert.Equal(newDateTime, field.LastSeen);
        }

        [Theory]
        [InlineData(FieldType.GoalArea)]
        [InlineData(FieldType.GoalCompleted)]
        [InlineData(FieldType.GoalFailed)]
        public void GoalAreaShamTest(FieldType expectedFieldType)
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            Data expectedData = CreateGoalAreaResponse(newDateTime, GoalFieldType.unknown);
            responseStack.Push(expectedData);
            playerGameState.Piece = new Core.Models.Piece() { IsSham = false, PlayerId = playerId };
            var field = playerGameState.Board.Fields[playerGameState.Position.X, playerGameState.Position.Y];
            field.Type = expectedFieldType;
            field.LastSeen = oldDateTime;
            // Act
            var data = player.MakeAction(new PlacePiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.Null(playerGameState.Piece);
            Assert.Equal(expectedFieldType, field.Type);
            Assert.Equal(oldDateTime, field.LastSeen);
        }

        [Fact]
        public void TaskAreaTest()
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            playerGameState.Piece = new Core.Models.Piece() { IsSham = false, PlayerId = playerId, Id = 10 };
            Data expectedData = new Data()
            {
                TaskFields = new TaskFieldDto[] { new TaskFieldDto() { pieceIdSpecified = true, pieceId = playerGameState.Piece.Id, timestamp = newDateTime } }
            };
            responseStack.Push(expectedData);
            var field = playerGameState.Board.Fields[playerGameState.Position.X, playerGameState.Position.Y];
            field.Type = FieldType.TaskArea;
            field.LastSeen = oldDateTime;

            // Act
            var data = player.MakeAction(new PlacePiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.Null(playerGameState.Piece);
            Assert.Equal(newDateTime, field.LastSeen);
        }


        private Data CreateGoalAreaResponse(DateTime dateTime, GoalFieldType goalFieldType)
        {
            return new Data()
            {
                GoalFields = new GoalFieldDto[] { new GoalFieldDto() { type = goalFieldType, timestamp = dateTime, x = (uint)playerGameState.Position.X, y = (uint)playerGameState.Position.Y } }
            };
        }

    }
}
