﻿using Moq;
using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ProjectGame.UnitTests.PlayerTests.MakeActionTests
{
    public class DestroyPieceTests : MakeActionTestsBase
    {
        private DestroyPiece destroyPieceRequest;

        public DestroyPieceTests()
        {
            mockAdapter.Setup(a => a.MakeDestroyPieceRequest(It.IsAny<DestroyPiece>())).Callback((DestroyPiece req) => destroyPieceRequest = req);
        }

        [Fact]
        public void NoPieceTest()
        {
            // Arrange
            playerGameState.Piece = null;
            var destroyPiece = new DestroyPiece();

            // Act & Assert
            Assert.Throws<InvalidOperationException>(() => player.MakeAction(destroyPiece));
        }

        [Fact]
        public void CarryingPieceTest()
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            var expectedData = new Data()
            {
                Pieces = new PieceDto[] { new PieceDto() },
            };
            responseStack.Push(expectedData);
            playerGameState.Piece = new Core.Models.Piece();

            // Act
            var data = player.MakeAction(new DestroyPiece());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, destroyPieceRequest);
            Assert.Null(playerGameState.Piece);
        }
    }
}
