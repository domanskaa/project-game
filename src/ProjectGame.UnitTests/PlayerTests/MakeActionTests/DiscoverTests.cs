﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Models;

namespace ProjectGame.UnitTests.PlayerTests.MakeActionTests
{
    public class DiscoverTests : MakeActionTestsBase
    {
        protected Discover placePieceRequest;

        public DiscoverTests()
        {
            mockAdapter.Setup(a => a.MakeDiscoverRequest(It.IsAny<Discover>())).Callback((Discover req) => placePieceRequest = req);
        }

        [Fact]
        public void TaskFieldsDiscoverTest()
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            int manhatanDistance = 2;
            var expectedData = new Data()
            {
                TaskFields = new TaskFieldDto[] { new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y,
                    timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                },
            };
            responseStack.Push(expectedData);
            playerGameState.Position = oldPosition;
            var field = playerGameState.Board.Fields[oldPosition.X + 1, oldPosition.Y + 1];
            field.ManhattanDistance = manhatanDistance + 5;
            field.LastSeen = oldDateTime;

            // Act
            var data = player.MakeAction(new Discover());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.Equal(newDateTime, field.LastSeen);
            Assert.Equal(manhatanDistance, field.ManhattanDistance);
        }
        //TODO: edit test
        //[Fact]
        //public void PieceOnTaskFieldTest()
        //{
        //    // Arrange
        //    FakeConnectToGame(gameId, guid);
        //    int manhatanDistance = 2;
        //    ulong pieceId = 1;
        //    var expectedData = new Data()
        //    {
        //        TaskFields = new TaskFieldDto[] { new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y,
        //            timestamp = newDateTime, distanceToPiece = manhatanDistance },
        //            new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y, timestamp = newDateTime, distanceToPiece = manhatanDistance },
        //            new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y, timestamp = newDateTime, distanceToPiece = manhatanDistance },
        //            new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
        //            new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
        //            new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
        //            new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
        //            new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
        //            new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = 0,
        //            pieceIdSpecified = true, pieceId = pieceId},
        //        },
        //        Pieces = new PieceDto[] {new PieceDto() {id = pieceId} }
        //    };
        //    responseStack.Push(expectedData);
        //    playerGameState.Position = oldPosition;
        //    var field = playerGameState.Board.Fields[oldPosition.X + 1, oldPosition.Y + 1];
        //    field.ManhattanDistance = manhatanDistance;
        //    field.LastSeen = oldDateTime;
        //    field.Piece = null;

        //    // Act
        //    var data = player.MakeAction(new Discover());

        //    // Assert
        //    AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
        //    Assert.Equal(pieceId, field.Piece.Id);
        //}

        [Fact]
        public void PlayerCarryingPieceTest()
        {
            // Arrange
            FakeConnectToGame(gameId, guid);
            int manhatanDistance = 2;
            ulong pieceId = 1;
            ulong playerId = 1;
            var expectedData = new Data()
            {
                TaskFields = new TaskFieldDto[] { new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y,
                    timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y-1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X-1, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = manhatanDistance },
                    new TaskFieldDto() { x = (uint)oldPosition.X+1, y = (uint)oldPosition.Y+1, timestamp = newDateTime, distanceToPiece = 0,
                    pieceIdSpecified = true, pieceId = pieceId, playerIdSpecified = true, playerId = playerId},
                },
                Pieces = new PieceDto[] { new PieceDto() { id = pieceId, playerIdSpecified = true, playerId = playerId } }
            };
            responseStack.Push(expectedData);
            playerGameState.Position = oldPosition;
            var field = playerGameState.Board.Fields[oldPosition.X + 1, oldPosition.Y + 1];
            field.ManhattanDistance = manhatanDistance + 5;
            field.LastSeen = oldDateTime;
            field.Piece = new Piece();

            // Act
            var data = player.MakeAction(new Discover());

            // Assert
            AssertCredentialsSet(gameId, guid, expectedData, data, placePieceRequest);
            Assert.Equal(0, field.ManhattanDistance);
            Assert.Equal(playerId, field.Player.Id);
            Assert.Null(field.Piece);
            Assert.True(field.IsOccupied);
        }


    }
}
