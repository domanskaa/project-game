using System;
using System.Collections.Generic;
using Xunit;
using ProjectGame.GameMaster;
using ProjectGame.Core;
using ProjectGame.Core.Models.Configuration;
using System.Xml.Serialization;
using System.IO;
using ProjectGame.Core.Models;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Models.Messages;
using ProjectGame.GameMaster.Enum;

namespace ProjectGame.UnitTests.ValidationTests
{
    public class JoinGameValidationTests: ActionValidationTests
    {
        [Fact]
        public void JoinFullTeamTest()
        {
            //Arrange
            _numberOfPlayersPerTeam = 1;
            JoinGame joinGame = new JoinGame(){
                playerId = 3,
                preferredRole = PlayerType.member,
                preferredTeam = MessageTeamColour.blue
            };
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(joinGame, JoinGameValidationScope.Team));
        }

        [Fact]
        public void JoinBusyRoleTest()
        {
            //Arrange
            _numberOfPlayersPerTeam = 1;
            JoinGame joinGame = new JoinGame(){
                playerId = 3,
                preferredRole = PlayerType.leader,
                preferredTeam = MessageTeamColour.blue
            };
            _gameState.TeamBlue[0].IsLeader = true;
            _gameState.TeamRed[0].IsLeader = true;
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(joinGame, JoinGameValidationScope.Role));
        }
    }
}