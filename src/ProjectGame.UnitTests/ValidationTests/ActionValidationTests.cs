using System;
using System.Collections.Generic;
using Xunit;
using ProjectGame.GameMaster;
using ProjectGame.Core;
using ProjectGame.Core.Models.Configuration;
using System.Xml.Serialization;
using System.IO;
using ProjectGame.Core.Models;
using ProjectGame.Core.Enum;

namespace ProjectGame.UnitTests.ValidationTests
{
    public class ActionValidationTests
    {
        protected GameState _gameState;
        protected int _numberOfPlayersPerTeam;
        
        public ActionValidationTests()
        {
            _numberOfPlayersPerTeam = 2;
            _gameState = new GameState(){
                Board = GetExampleBoard(),
                TeamBlue = new List<PlayerInfo>(),
                TeamRed = new List<PlayerInfo>()
            };
            _gameState.TeamBlue.Add(GetExamplePlayer(1, PlayerColor.Blue));
            _gameState.TeamRed.Add(GetExamplePlayer(2, PlayerColor.Red));
        }

        private PlayerInfo GetExamplePlayer(uint id, PlayerColor color)
        {
            int blueY = 1;
            int redY = (int)_gameState.Board.BoardHeight - 2;
            Position position = new Position(1, color == PlayerColor.Blue ? blueY : redY);
            PlayerInfo player = new PlayerInfo(){
                Id = id,
                Guid = Guid.NewGuid(),
                IsLeader = false,
                Piece = null,
                Position = position,
                Color = color
            };
             _gameState.Board.GetField(position).Player = player;
            return player;
        }
        private Board GetExampleBoard()
        {
            Board board = new Board(2, 4, 4);
            InitializeFields(board);
            return board;
        }

        private void InitializeFields(Board board)
        {
            for (uint i = 0; i < board.BoardWidth; i++)
            {
                for (uint j = 0; j < board.BoardHeight; j++)
                {
                    board.Fields[i, j].TeamOwnership = TeamOwnership.None;
                    if (j < board.GoalAreaHeight)
                    {
                        board.Fields[i, j].Type = FieldType.GoalArea;
                        board.Fields[i, j].TeamOwnership = TeamOwnership.Blue;
                    }
                    else if (j >= board.GoalAreaHeight + board.TaskAreaHeight)
                    {
                        board.Fields[i, j].Type = FieldType.GoalArea;
                        board.Fields[i, j].TeamOwnership = TeamOwnership.Red;
                    }
                    else
                    {
                        board.Fields[i, j].Type = FieldType.TaskArea;
                    }
                }
            }

            board.Fields[1,0].Type = FieldType.GoalNotDiscovered;
            board.Fields[2,0].Type = FieldType.GoalNotDiscovered;
            board.Fields[1, board.BoardHeight - 1].Type = FieldType.GoalNotDiscovered;
            board.Fields[2, board.BoardHeight - 1].Type = FieldType.GoalNotDiscovered;
        }

        protected void MovePlayer(PlayerInfo player, Position newPosition)
        {
            _gameState.Board.GetField(player.Position).Player = null;
            _gameState.Board.GetField(newPosition).Player = player;
            player.Position = newPosition;
        }

    }
}