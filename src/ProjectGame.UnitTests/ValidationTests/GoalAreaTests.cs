using System;
using Xunit;
using ProjectGame.GameMaster;
using ProjectGame.Core;
using ProjectGame.Core.Models.Configuration;
using System.Xml.Serialization;
using System.IO;

namespace ProjectGame.UnitTests.ValidationTests
{
    public class GoalAreaTests: ConfigurationValidationTests
    {
        [Fact]
        public void ZeroGoalAreaLength()
        {
            //Arrange
            _gameMasterSettings.GameDefinition.GoalAreaLength = 0;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void InvalidNumberOfPlayersTest()
        {
            //Arrange
            uint width = _gameMasterSettings.GameDefinition.BoardWidth;
            uint length = _gameMasterSettings.GameDefinition.GoalAreaLength;
            _gameMasterSettings.GameDefinition.NumberOfPlayersPerTeam = width * length + 1;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void InvalidNumberOfGoalsTest()
        {
            //Arrange
            uint goalsCount = (uint)_gameMasterSettings.GameDefinition.Goals.Length;
            _gameMasterSettings.GameDefinition.BoardWidth = (uint)(goalsCount / 2) - 1;
            _gameMasterSettings.GameDefinition.GoalAreaLength = 1;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void UnequalGoalsCountTest()
        {
            //Arrange
            GoalField[] newGoals = new GoalField[]
            {
                new GoalField(){
                    x = 0,
                    y = 0,
                    team = TeamColour.blue
                }
            };
            _gameMasterSettings.GameDefinition.Goals = newGoals;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void GoalPositionOutOfBoardTest()
        {
            //Arrange
            uint x = _gameMasterSettings.GameDefinition.BoardWidth;
            uint y = _gameMasterSettings.GameDefinition.TaskAreaLength + _gameMasterSettings.GameDefinition.GoalAreaLength * 2;
            GoalField[] newGoals = new GoalField[]
            {
                new GoalField(){
                    x = x,
                    y = y,
                    team = TeamColour.blue
                },
                new GoalField(){
                    x = x + 1,
                    y = y + 1,
                    team = TeamColour.red
                }
            };
            _gameMasterSettings.GameDefinition.Goals = newGoals;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void GoalInInvalidAreaTest()
        {
            //Arrange
            uint x = _gameMasterSettings.GameDefinition.BoardWidth;
            uint y = _gameMasterSettings.GameDefinition.TaskAreaLength + _gameMasterSettings.GameDefinition.GoalAreaLength * 2;
            GoalField[] newGoals = new GoalField[]
            {
                new GoalField(){
                    x = x - 2,
                    y = y - 1,
                    team = TeamColour.blue
                },
                new GoalField(){
                    x = x - 1,
                    y = y - 1,
                    team = TeamColour.red
                }
            };
            _gameMasterSettings.GameDefinition.Goals = newGoals;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void GoalsEqualLocations()
        {
             //Arrange
             GoalField[] goals = _gameMasterSettings.GameDefinition.Goals;
             int goalsCount = goals.Length;
             goals[goalsCount - 1].x = goals[goalsCount -2].x;
             goals[goalsCount - 1].y = goals[goalsCount -2].y;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }
    }
}     