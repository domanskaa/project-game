using System;
using Xunit;
using ProjectGame.GameMaster;
using ProjectGame.Core;
using ProjectGame.Core.Models.Configuration;
using System.Xml.Serialization;
using System.IO;

namespace ProjectGame.UnitTests.ValidationTests
{
    public class ParametersPresenceTests: ConfigurationValidationTests
    {
        [Fact]
        public void NoActionCostsTest()
        {
            //Arrange
            _gameMasterSettings.ActionCosts = null;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void NoGameDefinitionTest()
        {
            //Arrange
            _gameMasterSettings.GameDefinition = null;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void NoGoalsTest()
        {
            //Arrange
            _gameMasterSettings.GameDefinition.Goals = null;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void NoGameNameTest(string gameName)
        {
             //Arrange
            _gameMasterSettings.GameDefinition.GameName = null;
            //Act&Assert
            Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }
    }
}       
        
       