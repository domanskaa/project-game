using System;
using Xunit;
using ProjectGame.GameMaster;
using ProjectGame.Core;
using ProjectGame.Core.Models.Configuration;
using System.Xml.Serialization;
using System.IO;

namespace ProjectGame.UnitTests.ValidationTests
{
    public class ConfigurationValidationTests
    {
        protected GameMasterSettings _gameMasterSettings;
        
        public ConfigurationValidationTests()
        {
            string filename = "../../../XML/Configuration.xml";
            _gameMasterSettings = new XMLConfigurationLoader<GameMasterSettings>(filename).GetConfiguration();
        }

    }
}