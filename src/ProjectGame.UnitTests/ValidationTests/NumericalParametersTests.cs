using System;
using Xunit;
using ProjectGame.GameMaster;
using ProjectGame.Core;
using ProjectGame.Core.Models.Configuration;
using System.Xml.Serialization;
using System.IO;

namespace ProjectGame.UnitTests.ValidationTests
{
    public class NumeicalParametersTests: ConfigurationValidationTests
    {
        [Theory]
        [InlineData(-0.5D)]
        [InlineData(2D)]
        public void InvalidShamProbabilityTest(double shamProbability)
        {
            //Arrange
            _gameMasterSettings.GameDefinition.ShamProbability = shamProbability;
            //Act&Assert
             Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Theory]
        [InlineData(1000)]
        public void InvalidInitialNumberOfPiecesTest(uint initialNumberOfPieces)
        {
            //Arrange
            _gameMasterSettings.GameDefinition.InitialNumberOfPieces = initialNumberOfPieces;
            //Act&Assert
             Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void ZeroBoardWidthTest()
        {
            //Arrange
            _gameMasterSettings.GameDefinition.BoardWidth = 0;
            //Act&Assert
             Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void ZeroTaskAreaLengthTest()
        {
            //Arrange
            _gameMasterSettings.GameDefinition.TaskAreaLength = 0;
            //Act&Assert
             Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void ZeroPlacingNewPiecesFrequencyTest()
        {
            //Arrange
            _gameMasterSettings.GameDefinition.PlacingNewPiecesFrequency = 0;
            //Act&Assert
             Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

        [Fact]
        public void ZeroNumberOfPlayersPerTeamTest()
        {
            //Arrange
            _gameMasterSettings.GameDefinition.NumberOfPlayersPerTeam = 0;
            //Act&Assert
             Assert.False(ConfigurationValidator.Validate(_gameMasterSettings, out string message));
        }

    }
}       
        
       