using System;
using System.Collections.Generic;
using Xunit;
using ProjectGame.GameMaster;
using ProjectGame.Core;
using ProjectGame.Core.Models.Configuration;
using System.Xml.Serialization;
using System.IO;
using ProjectGame.Core.Models;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Models.Messages;
using ProjectGame.GameMaster.Enum;

namespace ProjectGame.UnitTests.ValidationTests
{
    public class SimpleActionsValidationTests: ActionValidationTests
    {
        [Fact]
        public void PlaceNoPieceTest()
        {
            //Arrange
            PlayerInfo player = _gameState.TeamBlue[0];
            PlacePiece placePiece = new PlacePiece(){
                playerGuid = player.Guid.ToString()
            };
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(placePiece));
        }

        [Fact]
        public void PlacePieceOnAnotherTest()
        {
            //Arrange
            PlayerInfo player = _gameState.TeamBlue[0];
            MovePlayer(player, new Position(2,2));
            _gameState.Board.GetField(player.Position).Piece = new Piece();
            PlacePiece placePiece = new PlacePiece(){
                playerGuid = player.Guid.ToString()
            };
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(placePiece));
        }

        [Fact]
        public void PickUpNoPieceTest()
        {
            //Arrange
            PlayerInfo player = _gameState.TeamBlue[0];
            PickUpPiece pickUpPiece = new PickUpPiece(){
                playerGuid = player.Guid.ToString()
            };
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(pickUpPiece));
        }

        [Fact]
        public void TestNoPieceTest()
        {
            //Arrange
            PlayerInfo player = _gameState.TeamBlue[0];
            TestPiece testPiece = new TestPiece(){
                playerGuid = player.Guid.ToString()
            };
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(testPiece));
        }

        [Fact]
        public void DestroyNoPieceTest()
        {
            //Arrange
            PlayerInfo player = _gameState.TeamBlue[0];
            DestroyPiece destroyPiece = new DestroyPiece(){
                playerGuid = player.Guid.ToString()
            };
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(destroyPiece));
        }
    }
}