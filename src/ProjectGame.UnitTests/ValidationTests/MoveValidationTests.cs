using System;
using System.Collections.Generic;
using Xunit;
using ProjectGame.GameMaster;
using ProjectGame.Core;
using ProjectGame.Core.Models.Configuration;
using System.Xml.Serialization;
using System.IO;
using ProjectGame.Core.Models;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Models.Messages;
using ProjectGame.GameMaster.Enum;

namespace ProjectGame.UnitTests.ValidationTests
{
    public class MoveValidationTests: ActionValidationTests
    {
        [Fact]
        public void MoveOutOfBoardTest()
        {
            //Arrange
            PlayerInfo player = _gameState.TeamBlue[0];
            MovePlayer(player, new Position(0,0));
            Move move = new Move(){
                playerGuid = player.Guid.ToString(),
                direction = MoveType.left
            };
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(move, MoveValidationScope.Edge));
        }

        [Fact]
        public void MoveOnPlayerTest()
        {
            //Arrange
            PlayerInfo bluePlayer = _gameState.TeamBlue[0];
            PlayerInfo redPlayer = _gameState.TeamRed[0];
            MovePlayer(bluePlayer, new Position(2,3));
            MovePlayer(redPlayer, new Position(2,4));
            Move move = new Move(){
                playerGuid = bluePlayer.Guid.ToString(),
                direction = MoveType.up
            };
            ActionValidator validator = new ActionValidator(_gameState, _numberOfPlayersPerTeam);
            //Act & Assert
            Assert.False(validator.Validate(move, MoveValidationScope.Player));
        }

    }
}