from subprocess import Popen
import time

p_cs = Popen(["dotnet", "./ProjectGame.CommunicationServer/bin/Debug/netcoreapp2.0/ProjectGame.CommunicationServer.dll", "--port", "4444", "--conf", "./ProjectGame.UnitTests/XML/CommunicationServerConfiguration.xml"], stdout=open("cs_output.log.txt", "w"))

p_cs.wait()
