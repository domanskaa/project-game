﻿using Microsoft.Extensions.CommandLineUtils;
using ProjectGame.CommunicationServer.Interfaces;
using ProjectGame.Core;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Exceptions;
using ProjectGame.Core.Interfaces;
using ProjectGame.Core.Models.Configuration;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;

namespace ProjectGame.PlayerProject
{
    class Program
    {
        static string address = null;
        static string gameName = null;
        static PlayerType preferredPlayerType = PlayerType.leader;
        static PlayerColor preferredColour = PlayerColor.Unknown;
        static int port = -1;
        static PlayerSettings settings;
        static string XMLPath = "";

        static void Main(string[] args)
        {
            bool shouldRun = false;
            var app = new CommandLineApplication();
            CommandOption addressOption, portOption, gameNameOption, preferredRoleOption, preferredTeamOption, xmlPathOption;
            InitOptions(app, out addressOption, out portOption, out gameNameOption, out preferredRoleOption, out preferredTeamOption, out xmlPathOption);
            app.OnExecute(() =>
            {
                ParseOptions(app, addressOption, portOption, gameNameOption, preferredRoleOption, preferredTeamOption, xmlPathOption);

                shouldRun = true;
                return 0;
            });
            try
            {
                app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.WriteLine(ex.Message);
                app.ShowHint();
            }
            if (shouldRun)
                RunPlayer();

            Console.WriteLine("Close app.");

        }

        private static void InitOptions(CommandLineApplication app, out CommandOption addressOption, out CommandOption portOption, out CommandOption gameNameOption, out CommandOption preferredRoleOption, out CommandOption preferredTeamOption, out CommandOption xmlPath)
        {
            app.Name = "PlayerProject";
            app.Description = ".NET Core application simulating agent in 'The Project Game'";
            var helpOption = app.HelpOption("-h|--help");
            addressOption = app.Option("--address <value>",
                    "server IPv4 address or IPv6 address or host name",
                    CommandOptionType.SingleValue);
            portOption = app.Option("--port <value>",
                    "server port number",
                    CommandOptionType.SingleValue);
            gameNameOption = app.Option("--game <value>",
                    "name of the game",
                    CommandOptionType.SingleValue);
            preferredRoleOption = app.Option("--role <value>",
                    "preferred role [leader|member]",
                    CommandOptionType.SingleValue);
            preferredTeamOption = app.Option("--team <value>",
                    "preferred team [red|blue]",
                    CommandOptionType.SingleValue);
            xmlPath = app.Option("--conf <value>",
                    "path to XML configuration file",
                    CommandOptionType.SingleValue);
        }

        private static void ParseOptions(CommandLineApplication app, CommandOption addressOption, CommandOption portOption, CommandOption gameNameOption, CommandOption preferredRoleOption, CommandOption preferredTeamOption,CommandOption xmlPath)
        {
            var roles = new[] { "leader", "member" };
            var teams = new[] { "red", "blue" };

            address = addressOption.Value();
            gameName = gameNameOption.Value();
            var preferredRole = preferredRoleOption.Value();
            var preferredTeam = preferredTeamOption.Value();

            if (address == null)
                throw new CommandParsingException(app, "Invalid address. Aborting");

            if (!int.TryParse(portOption.Value(), out port))
                throw new CommandParsingException(app, "Invalid port number. Aborting");

            if (!roles.Contains(preferredRole))
                throw new CommandParsingException(app, "Invalid preferred role. Aborting");
            if (!teams.Contains(preferredTeam))
                throw new CommandParsingException(app, "Invalid preferred team. Aborting");

            preferredColour = preferredTeam == teams[0] ? PlayerColor.Red : PlayerColor.Blue;
            preferredPlayerType = preferredRole == roles[0] ? PlayerType.leader : PlayerType.member;

            XMLPath = xmlPath.Value();
            
            if (XMLPath == null)
                throw new CommandParsingException(app, "Invalid path to configuration file. Aborting");

            try
            {
                settings = new XMLConfigurationLoader<PlayerSettings>(XMLPath).GetConfiguration();
            }
            catch (FileNotFoundException)
            {
                throw new CommandParsingException(app, "Configuration file could not be found. Aborting.");
            }
        }

        public static void RunPlayer()
        {
            TcpClient con = null;

            while(con == null)
            {
                try
                {
                    con = new TcpClient(address, port);

                } catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Thread.Sleep((int)settings.RetryJoinGameInterval);
                }
            }

            try
            {
                ICommunicationClient client = new TcpSocketCommunicationClient(con, sendKeepAlives:true, verbose: false, timeout: (int)settings.KeepAliveInterval);
                ISerializer serializer = new MultipleTypesXmlSerializer();
                IPlayerAdapter adapter = new PlayerNetAdapter(client, serializer);
                var strategy = new SimpleStrategy();

                var player = new Player(gameName, preferredPlayerType, preferredColour, adapter, strategy);
                player.Run();
            }
            catch (CommunicationClientException)
            {
                Console.WriteLine("Connection lost with communication server");
            }
        }
    }
}
