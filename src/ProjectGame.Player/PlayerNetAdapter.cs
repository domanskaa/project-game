﻿using ProjectGame.CommunicationServer.Interfaces;
using ProjectGame.Core.Interfaces;
using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.PlayerProject
{
    public class PlayerNetAdapter : IPlayerAdapter
    {
        private readonly ICommunicationClient _communicationClient;
        private readonly ISerializer _serializer;
        public PlayerNetAdapter(ICommunicationClient communicationClient, ISerializer serializer)
        {
            _communicationClient = communicationClient;
            _serializer = serializer;
        }

        public void MakeDestroyPieceRequest(DestroyPiece destroyPieceData)
        {
            MakeRequest(destroyPieceData, typeof(DestroyPiece));
        }

        public void MakeDiscoverRequest(Discover discoverData)
        {
            MakeRequest(discoverData, typeof(Discover));
        }

        public void MakeGetGamesReuqest(GetGames getGames)
        {
            MakeRequest(getGames, typeof(GetGames));
        }

        public void MakeJoinGameRequest(JoinGame joinGame)
        {
            MakeRequest(joinGame, typeof(JoinGame));
        }

        public void MakeMoveRequest(Move moveResponseData)
        {
            MakeRequest(moveResponseData, typeof(Move));
        }

        public void MakePickUpPieceRequest(PickUpPiece pickUpPieceData)
        {
            MakeRequest(pickUpPieceData, typeof(PickUpPiece));
        }

        public void MakePlacePieceRequest(PlacePiece placePieceData)
        {
            MakeRequest(placePieceData, typeof(PlacePiece));
        }

        public void MakeTestRequest(TestPiece testResponseData)
        {
            MakeRequest(testResponseData, typeof(TestPiece));
        }

        public object Receive()
        {
            var bytes = _communicationClient.Receive();
            return _serializer.Deserialize(bytes);
        }

        private void MakeRequest(object obj, Type type)
        {
            var bytes = _serializer.Serialize(obj, type);

            _communicationClient.Send(bytes);
        }
    }
}
