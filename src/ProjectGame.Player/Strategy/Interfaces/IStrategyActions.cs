﻿using ProjectGame.Core.Models.Messages;

namespace ProjectGame.PlayerProject
{
    public interface IStrategyActions
    {
        Move MakeMoveAction(MoveType moveType);
        Discover MakeDiscoverAction();
        PickUpPiece MakePickUpPieceAction();
        PlacePiece MakePlacePieceAction();
        DestroyPiece MakeDestroyPieceAction();
        TestPiece MakeTestPieceAction();
    }
}