﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.PlayerProject
{
    public interface IStrategy
    {
        object GetAction(PlayerGameState gameState);
    }
}
