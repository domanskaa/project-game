﻿using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.PlayerProject
{
    public class SimpleStrategyActions : PreviousStateMemory, IStrategyActions
    {
        public DestroyPiece MakeDestroyPieceAction()
        {
            var destroyPiece = new DestroyPiece();

            return RememberAndReturnAction(destroyPiece);
        }

        public Discover MakeDiscoverAction()
        {
            var discover = new Discover();

            return RememberAndReturnAction(discover);
        }

        public Move MakeMoveAction(MoveType moveType)
        {
            var move = new Move()
            {
                direction = moveType,
                directionSpecified = true
            };
            _lastMoveType = moveType;

            return RememberAndReturnAction(move);
        }

        public PickUpPiece MakePickUpPieceAction()
        {
            var pickUpPiece = new PickUpPiece();
            return RememberAndReturnAction(pickUpPiece);
        }

        public PlacePiece MakePlacePieceAction()
        {
            var placePiece = new PlacePiece();
            return RememberAndReturnAction(placePiece);
        }

        public TestPiece MakeTestPieceAction()
        {
            var testPiece = new TestPiece();
            return RememberAndReturnAction(testPiece);
        }

        private T RememberAndReturnAction<T>(T action)
        {
            _previousGameState = _currentGameState.Clone() as PlayerGameState;
            _lastAction = action;
            return action;
        }
        protected void LastActionFlagsUpdate()
        {
            if(_lastAction != null && _lastAction is Move)
                _isLastMoveValid = !_previousGameState.Position.Equals(_currentGameState.Position);
        }
    }
}
