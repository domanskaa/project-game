﻿using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.PlayerProject
{
    public class PreviousStateMemory : BasicMemory
    {
        protected PlayerGameState _previousGameState;
        protected object _lastAction { get; set; }
        protected MoveType _lastMoveType { get; set; }
        protected bool _isLastMoveValid { get; set; }
        protected bool _isLastActionDiscover => _lastAction != null && _lastAction is Discover;
    }
}
