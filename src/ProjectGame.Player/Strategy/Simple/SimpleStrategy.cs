﻿using ProjectGame.Core.Enum;
using ProjectGame.Core.Models;
using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.PlayerProject
{
    public class SimpleStrategy : SimpleStrategyActions, IStrategy 
    {
        public object GetAction(PlayerGameState gameState)
        {
            _currentGameState = gameState;
            LastActionFlagsUpdate();

            if (CurrentField.InGoalArea)
            {
                if (IsCarryingPiece)
                {
                    if (CurrentField.Type == FieldType.GoalArea)
                        return MakePlacePieceAction();

                    if (_isLastMoveValid)
                        return MakeMoveAction(_lastMoveType);

                    return MakeMoveAction(CalculateNewMove(_lastMoveType));
                }

                return MoveTowardsTaskArea();
            }
            else
            {
                if (IsCarryingPiece)
                {
                    if (!_isLastMoveValid)
                        return MakeMoveAction(CalculateNewMove(_lastMoveType));
                    return MoveTowardsGoalArea();
                }

                if (CurrentField.ManhattanDistance == 0)
                    return MakePickUpPieceAction();

                if (!_isLastMoveValid)
                    return MakeMoveAction(CalculateNewMove(_lastMoveType));

                if (_isLastActionDiscover)
                    return MakeMoveAction(GetMoveNearestToPiece());
                else
                    return MakeDiscoverAction();
            }
        }

        private object MoveTowardsTaskArea()
        {
            if (_currentGameState.Color == PlayerColor.Blue)
                return MakeMoveAction(MoveType.up);
            else
                return MakeMoveAction(MoveType.down);
        }
        private object MoveTowardsGoalArea()
        {
            if (_currentGameState.Color == PlayerColor.Blue)
                return MakeMoveAction(MoveType.down);
            else
                return MakeMoveAction(MoveType.up);
        }

        private MoveType GetMoveNearestToPiece()
        {
            var moveType = MoveType.up;
            var nearestMove = MoveType.up;
            int mannhattanDistance = int.MaxValue;

            while (true)
            {
                var position = CalculatePosition(moveType);
                if (position.X < _currentGameState.Board.BoardWidth && position.X >= 0 && position.Y < _currentGameState.Board.BoardHeight && position.Y >= 0)
                {
                    var field = _currentGameState.Board.Fields[position.X, position.Y];
                    if (field.ManhattanDistance.HasValue && mannhattanDistance > field.ManhattanDistance)
                    {
                        mannhattanDistance = field.ManhattanDistance.Value;
                        nearestMove = moveType;
                    }
                }
                moveType = CalculateNewMove(moveType);
                if (moveType == MoveType.up)
                    break;
            }

            return nearestMove;
        }
        private MoveType CalculateNewMove(MoveType lastMoveType)
        {
            switch (lastMoveType)
            {
                case MoveType.down:
                    return MoveType.left;
                case MoveType.up:
                    return MoveType.right;
                case MoveType.right:
                    return MoveType.down;
                case MoveType.left:
                    return MoveType.up;
                default:
                    throw new InvalidOperationException();
            }
        }
        private Position CalculatePosition(MoveType moveType)
        {
            var newPosition = _currentGameState.Position;
            switch (moveType)
            {
                case MoveType.down:
                    newPosition.Y--;
                    break;
                case MoveType.up:
                    newPosition.Y++;
                    break;
                case MoveType.left:
                    newPosition.X--;
                    break;
                case MoveType.right:
                    newPosition.X++;
                    break;
            }

            return newPosition;
        }

        private Field CurrentField => _currentGameState.Board.Fields[_currentGameState.Position.X, _currentGameState.Position.Y];
        private bool IsCarryingPiece => _currentGameState.Piece != null;
    }
}
