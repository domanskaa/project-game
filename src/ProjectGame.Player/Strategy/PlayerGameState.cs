﻿using ProjectGame.Core.Enum;
using ProjectGame.Core.Models;
using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.PlayerProject
{
    public class PlayerGameState : ICloneable
    {
        public Position Position { get; set; }
        public Piece Piece { get; set; }
        public Board Board { get; set; }
        public PlayerColor Color { get; set; }

        public object Clone()
        {
            return new PlayerGameState()
            {
                Position = Position?.Clone() as Position,
                Piece = Piece?.Clone() as Piece,
                Board = Board?.Clone() as Board,
                Color = Color
            };

        }

    }
}
