﻿using ProjectGame.Core.Enum;
using ProjectGame.Core.Models;
using ProjectGame.Core.Models.Configuration;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ProjectGame.Core.Interfaces;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ProjectGame.PlayerProject
{
    public class Player
    {
        private ulong _gameId;
        private ulong _id;
        private string _guid;
        private IPlayerAdapter _adapter;
        private PlayerType _type;
        private PlayerMapInfo[] _players;
        private PlayerGameState _gameState;
        private IStrategy _strategy;
        
        public Player(IPlayerAdapter adapter, IStrategy strategy, PlayerGameState initialGameState = null)
        {
            _adapter = adapter;
            _gameState = initialGameState ?? new PlayerGameState();
            _strategy = strategy;
        }

        public Player(string gameName, PlayerType preferedPlayerType, PlayerColor preferedColour, IPlayerAdapter adapter, IStrategy strategy)
        {
            _adapter = adapter;
            _gameState = new PlayerGameState();
            _strategy = strategy;

            var request = new JoinGame()
            {
                gameName = gameName,
                preferredRole = preferedPlayerType,
                preferredTeam = preferedColour.ToMessageTeamColour()
            };

            ConnectToGame(request);
        }

        public void ConnectToGame(JoinGame request)
        {
            _adapter.MakeJoinGameRequest(request);
            var response = Receive<ConfirmJoiningGame>();
            // We assume that request is successful.

            var details = Receive<Game>();
            Console.WriteLine("Rozpoczynam gre");
            // We assume that request is successful.

            ProcessGameDetails(details);
            IntializePlayerFromJoinGameResponse(response);
        }

        public void Run()
        {
            bool isGameFinished = false;
            Object action = null;
            Data data = null;
            while (!isGameFinished)
            {
                action = _strategy.GetAction(_gameState);
                data = MakeAction(action as dynamic);
                isGameFinished = data.gameFinished;
            }
        }
        
        public Data MakeAction(PlacePiece action)
        {
            EnsurePieceNotNull();
            SetMessageParameters<PlacePiece>(action);
            _adapter.MakePlacePieceRequest(action);
            Data response = ReceiveData();
            ProcessPlacePieceResponse(response);
            return response;
        }

        public Data MakeAction(DestroyPiece action)
        {
            EnsurePieceNotNull();
            SetMessageParameters<DestroyPiece>(action);
            _adapter.MakeDestroyPieceRequest(action);

            var response = ReceiveData();
            _gameState.Piece = null; // After destroy player (successful or not) cannot carry a piece.
            return response;
        }

        public Data MakeAction(Move action)
        {
            var prevPosition = new Position(_gameState.Position.X, _gameState.Position.Y);
            SetMessageParameters<Move>(action);
            _adapter.MakeMoveRequest(action);
            var response = ReceiveData();
            if (response.PlayerLocation != null)
            {
                _gameState.Position.X = (int)response.PlayerLocation.x;
                _gameState.Position.Y = (int)response.PlayerLocation.y;
            }
            ProcessTaskFields(response);
            ProcessGoalFields(response);

            return response;
        }


        public Data MakeAction(Discover action)
        {
            SetMessageParameters<Discover>(action);
            _adapter.MakeDiscoverRequest(action);

            var response = ReceiveData();
            ProcessTaskFields(response);
            ProcessGoalFields(response);
            return response;
        }

        public Data MakeAction(PickUpPiece action)
        {
            SetMessageParameters<PickUpPiece>(action);
            _adapter.MakePickUpPieceRequest(action);

            var response = ReceiveData();
            ProcessPickUpPieceResponse(response);

            return response;
        }

        public Data MakeAction(TestPiece action)
        {
            EnsurePieceNotNull();
            SetMessageParameters<TestPiece>(action);
            _adapter.MakeTestRequest(action);

            var response = ReceiveData();
            ProcessTestPieceResponse(response);

            return response;
        }

        public void ProcessGameDetails(Game details)
        {
            var boardDetails = details.Board;
            _gameState.Board = new Board(boardDetails.goalsHeight, boardDetails.width, boardDetails.tasksHeight);
            _gameState.Board.InitializeFields();
            _gameState.Position = new Position((int)details.PlayerLocation.x, (int)details.PlayerLocation.y);
            _players = details.Players.Select(dto => new PlayerMapInfo(dto.id, dto.team.ToPlayerColour(), null)).ToArray();
        }

        private void ProcessTaskFields(Data response)
        {
            if (response.TaskFields != null && response.TaskFields.Count() != 0)
            {
                foreach (TaskFieldDto taskField in response.TaskFields)
                {
                    var field = _gameState.Board.Fields[taskField.x, taskField.y];
                    field.TeamOwnership = TeamOwnership.None;
                    field.ManhattanDistance = taskField.distanceToPiece;
                    field.LastSeen = taskField.timestamp;
                    if (taskField.pieceIdSpecified)
                    {
                        var responsePiece = GetPiece(taskField.pieceId, response);
                        if (responsePiece != null && !responsePiece.playerIdSpecified)
                            field.Piece = new Piece(responsePiece.id, responsePiece.type.ToIsSham(), responsePiece.type.ToIsTested());
                        else
                            field.Piece = null;
                    }
                    else
                        field.Piece = null;
                    field.Player = taskField.playerIdSpecified ? new PlayerMapInfo(taskField.playerId, PlayerColor.Unknown, field.Piece) : null;
                }
            }
        }

        private void ProcessGoalFields(Data response)
        {
            if (response.GoalFields != null && response.GoalFields.Count() != 0)
            {
                foreach (GoalFieldDto goalField in response.GoalFields)
                {
                    var field = _gameState.Board.Fields[goalField.x, goalField.y];
                    field.LastSeen = goalField.timestamp;
                    field.TeamOwnership = goalField.team.ToTeamOwnership();
                    field.Player = goalField.playerIdSpecified ? new PlayerMapInfo(goalField.playerId, PlayerColor.Unknown, null) : null;
                    field.Piece = null;
                    field.ManhattanDistance = null;
                }
            }
        }

        private void ProcessTestPieceResponse(Data response)
        {
            if (response.Pieces != null && response.Pieces.Count() != 0)
            {
                var responsePiece = response.Pieces[0];
                SetPieceParameters(_gameState.Piece, responsePiece);
            }
        }

        private void ProcessPickUpPieceResponse(Data response)
        {
            if (response.Pieces != null && response.Pieces.Count() != 0)
            {
                var responsePiece = response.Pieces[0];
                if (_gameState.Piece == null)
                     _gameState.Piece = new Piece(responsePiece.id, responsePiece.type.ToIsSham(), responsePiece.type.ToIsTested());
            }
        }

        private PieceDto GetPiece(ulong id, Data response)
        {
            if (response.Pieces != null)
                return response.Pieces.FirstOrDefault(item => item.id == id);

            return null;
        }

        private void SetPieceParameters(Piece piece, PieceDto pieceParameters)
        {
            piece.Id = pieceParameters.id;
            piece.IsTested = pieceParameters.type != PieceType.unknown ? true : false;
            piece.IsSham = pieceParameters.type == PieceType.sham ? true : false;
        }

        private void SetMessageParameters<T>(T msg) where T : GameMessage, new()
        {
            msg.gameId = _gameId;
            msg.playerGuid = _guid;
        }

        private void IntializePlayerFromJoinGameResponse(ConfirmJoiningGame response)
        {
            _gameId = response.gameId;
            _guid = response.privateGuid;
            _id = response.playerId;
            _gameState.Piece = null;
            _type = response.PlayerDefinition.type;
           _gameState.Color = response.PlayerDefinition.team.ToPlayerColour();
            _gameState.Board.Fields[_gameState.Position.X, _gameState.Position.Y].Player = new PlayerMapInfo() { Id = _id, Color = _gameState.Color };
        }

        private void ProcessPlacePieceResponse(Data response)
        {
            var field = _gameState.Board.Fields[_gameState.Position.X, _gameState.Position.Y];
            if (IsPiecePlacedInGoalArea(response))
            {
                var goalField = response.GoalFields[0];
                if(goalField.type != Core.Models.Messages.GoalFieldType.unknown)
                {
                    field.Type = goalField.type.ToFieldType();
                    field.LastSeen = goalField.timestamp;
                }
                _gameState.Piece = null;
            }
            else if (IsPiecePlacedInTaskArea(response))
            {
                // Player doesn't carry piece anymore
                field.Piece = _gameState.Piece;
                field.LastSeen = response.TaskFields[0].timestamp;
                _gameState.Piece = null;
            }
        }

        private static bool IsPiecePlacedInTaskArea(Data response)
        {
            return (response.Pieces == null || response.Pieces.Length == 0) && (response.TaskFields != null && response.TaskFields.Length > 0);
        }

        private static bool IsPiecePlacedInGoalArea(Data response)
        {
            return response.GoalFields != null && response.GoalFields.Length > 0;
        }

        private Data ReceiveData() => Receive<Data>();

        private T Receive<T>() where T : class
        {
            var obj = _adapter.Receive();
            if (obj is PlayerDisconnected) return Receive<T>();

            if (!(obj is T))
                throw new InvalidOperationException();
            return obj as T;
        }

        private void EnsurePieceNotNull()
        {
            if (_gameState.Piece == null)
                throw new InvalidOperationException();
        }

    }
}
