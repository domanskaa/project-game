
using ProjectGame.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.PlayerProject;
using ProjectGame.Core.Models.Configuration;
using ProjectGame.Core.Enum;
using System.Linq;

namespace ProjectGame.GameMaster 
{
    public class GameState
    {
        public Board Board { get; set; }
        public List<PlayerInfo> TeamBlue;
        public List<PlayerInfo> TeamRed;
        public int TeamBlueCompletedTasksCount { get; set; }
        public int TeamRedCompletedTasksCount { get; set; }
        public bool IsGameFinished { get; set; }
        public IEnumerable<PlayerInfo> Players{
            get{
                return TeamBlue.Concat(TeamRed);
            }
        }

        public PlayerInfo GetPlayer(Guid guid)
        {
            return Players.Where(p => p.Guid == guid).FirstOrDefault();
        }
        public PlayerInfo GetPlayer(string stringGuid)
        {
            return Players.Where(p => p.Guid == Guid.Parse(stringGuid)).FirstOrDefault();
        }
        public PlayerInfo GetPlayer(ulong id)
        {
            return Players.Where(p => p.Id == id).FirstOrDefault();
        }

    }
}