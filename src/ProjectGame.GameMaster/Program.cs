﻿using ProjectGame.Core.Models.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using ProjectGame.Core;
using ProjectGame.Core.Models.Messages;
using System.Threading.Tasks;
using System.Threading;
using ProjectGame.CommunicationServer.Interfaces;
using ProjectGame.Core.Services;
using ProjectGame.Core.Interfaces;
using Microsoft.Extensions.CommandLineUtils;
using System.Net.Sockets;
using ProjectGame.Core.Exceptions;

namespace ProjectGame.GameMaster
{
    class Program
    {
        static string XMLPath = "";
        static string address = "";
        static GameMasterSettings gameMasterSettings;
        static int port;

        static void Main(string[] args)
        {
            bool shouldRun = false;
            var app = new CommandLineApplication();
            CommandOption addressOption, portOption, xmlPathOption;
            InitOptions(app, out addressOption, out portOption, out xmlPathOption);
            app.OnExecute(() =>
            {
                ParseOptions(app, addressOption, portOption, xmlPathOption);

                shouldRun = true;
                return 0;
            });

            try
            {
                app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.WriteLine(ex.Message);
                app.ShowHint();
            }

            if (shouldRun)
                RunGameMaster();

            Console.WriteLine("Close app.");
        }
        private static void InitOptions(CommandLineApplication app, out CommandOption addressOption, out CommandOption portOption, out CommandOption xmlPathOption)
        {
            app.Name = "GameMaster";
            app.Description = ".NET Core application simulating game master in 'The Project Game'";
            var helpOption = app.HelpOption("-h|--help");
            addressOption = app.Option("--address <value>",
                    "server IPv4 address or IPv6 address or host name",
                    CommandOptionType.SingleValue);
            portOption = app.Option("--port <value>",
                    "server port number",
                    CommandOptionType.SingleValue);
            xmlPathOption = app.Option("--conf <value>",
                    "path to XML configuration file",
                    CommandOptionType.SingleValue);
        }

        private static void ParseOptions(CommandLineApplication app, CommandOption addressOption, CommandOption portOption, CommandOption xmlPathOption)
        {
            address = addressOption.Value();
            XMLPath = xmlPathOption.Value();

            if (address == null)
                throw new CommandParsingException(app, "Invalid address. Aborting");

            if (!int.TryParse(portOption.Value(), out port))
                throw new CommandParsingException(app, "Invalid port number. Aborting");

            if (XMLPath == null)
                throw new CommandParsingException(app, "Invalid path to configuration file. Aborting");

            try
            {
                gameMasterSettings = new XMLConfigurationLoader<GameMasterSettings>(XMLPath).GetConfiguration();
            }
            catch (FileNotFoundException)
            {
                throw new CommandParsingException(app, "Configuration file could not be found. Aborting.");
            }
        }

        private static void RunGameMaster()
        {
            TcpClient con = null;

            while (con == null)
            {
                try
                {
                    con = new TcpClient(address, port);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Thread.Sleep((int)gameMasterSettings.RetryRegisterGameInterval);
                }
            }

            try
            {
                ICommunicationClient client = new TcpSocketCommunicationClient(con, sendKeepAlives:true, verbose: false, timeout:(int)gameMasterSettings.KeepAliveInterval);
                ISerializer serializer = new MultipleTypesXmlSerializer();
                IGameMasterAdapter adapter = new GameMasterNetAdapter(client, serializer);

                GameMaster gm = new GameMaster(gameMasterSettings, adapter);
            }
            catch (CommunicationClientException)
            {
                Console.WriteLine("Connection lost with communication server");
            }
        }
    }
}
