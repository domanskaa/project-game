using System;
using System.Runtime.Serialization;

namespace ProjectGame.GameMaster.Exceptions
{
    [Serializable]
    public class WrongConfiguration : Exception
    {
        public WrongConfiguration() 
            : base()
        { }
        public WrongConfiguration(string message) 
            : base(message) 
        { }

        protected WrongConfiguration(SerializationInfo info, StreamingContext ctxt) 
            : base(info, ctxt)
        { }
    }
}