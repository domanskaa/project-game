using System;
using System.Runtime.Serialization;

namespace ProjectGame.GameMaster.Exceptions
{
    [Serializable]
    public class NotExpectedResponse : Exception
    {
        public NotExpectedResponse() 
            : base()
        { }
        public NotExpectedResponse(string message) 
            : base(message) 
        { }

        protected NotExpectedResponse(SerializationInfo info, StreamingContext ctxt) 
            : base(info, ctxt)
        { }
    }
}