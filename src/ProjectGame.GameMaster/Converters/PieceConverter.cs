using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.Core.Models;
using ProjectGame.Core.Models.Messages;

namespace ProjectGame.GameMaster.Converters
{
    public static class PieceConverter
    {
        public static PieceDto ToPieceDto(this Piece piece)
        {
            return new PieceDto(){
                id = (ulong)piece.Id,
                playerIdSpecified = piece.PlayerId != null,
                playerId = piece.PlayerId != null ? piece.PlayerId.Value : 0,
                timestamp = DateTime.Now,
                type = PieceType.unknown
            };
        }
    }
}

