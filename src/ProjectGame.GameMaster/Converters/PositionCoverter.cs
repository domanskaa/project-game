using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.Core.Models;
using ProjectGame.Core.Models.Messages;

namespace ProjectGame.GameMaster.Converters
{
    public static class PositionConverter
    {
        public static LocationDto ToLocationDto(this Position position)
        {
            return new LocationDto(){
                x = (uint)position.X,
                y = (uint)position.Y
            };
        }
    }
}
