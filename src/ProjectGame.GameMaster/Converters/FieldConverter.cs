using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.Core.Models;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Enum;

namespace ProjectGame.GameMaster.Converters
{
    public static class FieldConverter
    {
        public static TaskFieldDto ToTaskFieldDto(this Field field)
        {
            return new TaskFieldDto(){
                distanceToPiece = field.ManhattanDistance.Value,
                pieceIdSpecified = field.Piece != null,
                playerId = field.Player != null ? field.Player.Id : 0,
                pieceId = field.Piece != null ? field.Piece.Id : 0,
                playerIdSpecified = field.Player != null,
                timestamp = DateTime.Now,
                x = (uint)field.Position.X,
                y = (uint)field.Position.Y
            };
        }
        public static GoalFieldDto ToGoalFieldDto(this Field field)
        {
            return new GoalFieldDto(){
                type = GoalFieldType.unknown,
                team = field.TeamOwnership == TeamOwnership.Blue ? MessageTeamColour.blue : MessageTeamColour.red,
                playerIdSpecified = field.Player != null,
                playerId = field.Player != null ? field.Player.Id : 0,
                timestamp = DateTime.Now,
                x = (uint)field.Position.X,
                y = (uint)field.Position.Y
            };
        }
        public static LocationDto ToLocationDto(this Field field)
        {
            return new LocationDto(){
                x = (uint)field.Position.X,
                y = (uint)field.Position.Y
            };
        }
    }
}
