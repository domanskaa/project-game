﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.GameMaster.Enum
{
    public enum MoveValidationScope
    {
        Edge,
        Player,
        Both
    }
}
