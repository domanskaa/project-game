﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.GameMaster.Enum
{
    public enum JoinGameValidationScope
    {
        Role,
        Team,
        Both
    }
}
