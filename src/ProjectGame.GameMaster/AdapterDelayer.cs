﻿using ProjectGame.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Models.Configuration;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using ProjectGame.Core.Exceptions;

namespace ProjectGame.GameMaster
{
    public class AdapterDelayer : IGameMasterAdapter
    {
        private IGameMasterAdapter _innerAdapter;
        private GameMasterSettings _gameMasterSettings;
        public AdapterDelayer(IGameMasterAdapter adapter, GameMasterSettings settings)
        {
            _innerAdapter = adapter;
            _gameMasterSettings = settings;
        }

        public void MakeConfirmJoiningGameRequest(ConfirmJoiningGame confirmJoiningGame)
        {
            _innerAdapter.MakeConfirmJoiningGameRequest(confirmJoiningGame);
        }

        public void MakeDestroyResponseRequest(Data destroyResponseData)
        {
            PerformActionOnNewThread(() =>
            {
                //Thread.Sleep(_gameMasterSettings.ActionCosts.NIEMA);
                _innerAdapter.MakeDestroyResponseRequest(destroyResponseData);
            });
        }

        public void MakeDiscoverResponseRequest(Data discoverResponseData)
        {
            PerformActionOnNewThread(() =>
            {
                Thread.Sleep((int)_gameMasterSettings.ActionCosts.DiscoverDelay);
                _innerAdapter.MakeDiscoverResponseRequest(discoverResponseData);
            });
        }

        public void MakeGameFinishedRequest(Data gameFinished)
        {
            _innerAdapter.MakeGameFinishedRequest(gameFinished);
        }

        public void MakeGameRequest(Game game)
        {
            _innerAdapter.MakeGameRequest(game);
        }

        //public void MakeGameStartedRequest(GameStarted gameStarted)
        //{
        //    _innerAdapter.MakeGameStartedRequest(gameStarted);
        //}

        public void MakeMoveResponseRequest(Data moveResponseData)
        {
            PerformActionOnNewThread(() =>
            {
                Thread.Sleep((int)_gameMasterSettings.ActionCosts.MoveDelay);
                _innerAdapter.MakeMoveResponseRequest(moveResponseData);
            });
        }

        public void MakePickUpResponseRequest(Data pickUpResponseData)
        {
            PerformActionOnNewThread(() => {
                Thread.Sleep((int)_gameMasterSettings.ActionCosts.PickUpDelay);
                _innerAdapter.MakePickUpResponseRequest(pickUpResponseData);
            });
        }

        public void MakePlaceResponseRequest(Data placeResponseData)
        {
            PerformActionOnNewThread(() =>
            {
                Thread.Sleep((int)_gameMasterSettings.ActionCosts.PlacingDelay);
                _innerAdapter.MakePlaceResponseRequest(placeResponseData);
            });
        }

        public void MakeRegisterGameRequest(RegisterGame registerGame)
        {
            _innerAdapter.MakeRegisterGameRequest(registerGame);
        }

        public void MakeRejectJoiningGameRequest(RejectJoiningGame rejectJoiningGame)
        {
            _innerAdapter.MakeRejectJoiningGameRequest(rejectJoiningGame);
        }

        public void MakeTestReponseRequest(Data testResponseData)
        {
            PerformActionOnNewThread(() =>
            {
                Thread.Sleep((int)_gameMasterSettings.ActionCosts.TestDelay);
                _innerAdapter.MakePlaceResponseRequest(testResponseData);
            });
        }

        private void PerformActionOnNewThread(Action action)
        {
            new Thread(() =>
            {
                try
                {
                    action();
                }
                catch (CommunicationClientException)
                {
                    Console.WriteLine("Connection lost.");
                }

            }).Start();
        }

        public object Receive()
        {
            return _innerAdapter.Receive();
        }
    }
}
