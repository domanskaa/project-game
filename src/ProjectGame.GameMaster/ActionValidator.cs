﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Models;
using ProjectGame.GameMaster.Enum;
using System.Linq;

namespace ProjectGame.GameMaster
{
    public class ActionValidator
    {
        GameState _gameState;
        int _numberOfPlayersPerTeam;
        public ActionValidator(GameState gameState, int numerOfPlayersPerTeam)
        {
            _gameState = gameState;
            _numberOfPlayersPerTeam = numerOfPlayersPerTeam;
        }

        public bool Validate(Move move, MoveValidationScope moveValidation)
        {
            var playerInfo = _gameState.GetPlayer(move.playerGuid);
            Position newPosition = new Position(playerInfo.Position.X, playerInfo.Position.Y);
            if (move.directionSpecified == false)
                return false;

            switch (move.direction)
            {
                case MoveType.down:
                    newPosition.Y--;
                    break;
                case MoveType.up:
                    newPosition.Y++;
                    break;
                case MoveType.left:
                    newPosition.X--;
                    break;
                case MoveType.right:
                    newPosition.X++;
                    break;
            }

            if (moveValidation == MoveValidationScope.Edge || moveValidation == MoveValidationScope.Both)
            {
                if (newPosition.X < 0 || newPosition.X >= _gameState.Board.BoardWidth)
                    return false;
                if (newPosition.Y < 0 || newPosition.Y >= _gameState.Board.BoardHeight)
                    return false;

                if (playerInfo.Color == PlayerColor.Blue)
                {
                    if (newPosition.Y < 0 || newPosition.Y >= _gameState.Board.GoalAreaHeight + _gameState.Board.TaskAreaHeight)
                        return false;
                }
                if (playerInfo.Color == PlayerColor.Red)
                {
                    if (newPosition.Y < _gameState.Board.GoalAreaHeight || newPosition.Y >= _gameState.Board.BoardHeight)
                        return false;
                }
            }

            if (moveValidation == MoveValidationScope.Player || moveValidation == MoveValidationScope.Both)
            {
                if (newPosition.X < 0 || newPosition.X >= _gameState.Board.BoardWidth)
                    return true;
                if (newPosition.Y < 0 || newPosition.Y >= _gameState.Board.BoardHeight)
                    return true;

                if (_gameState.Board.Fields[newPosition.X, newPosition.Y].IsOccupied)
                    return false;
            }

            return true;
        }

        public bool Validate(PickUpPiece pickUpPiece)
        {
            var playerInfo = _gameState.GetPlayer(pickUpPiece.playerGuid);

            if (_gameState.Board.Fields[playerInfo.Position.X, playerInfo.Position.Y].Piece != null && !playerInfo.HasPiece)
                return true;

            return false;
        }

        public bool Validate(PlacePiece placePiece)
        {
            var playerInfo = _gameState.GetPlayer(placePiece.playerGuid);

            if (playerInfo.HasPiece && _gameState.Board.Fields[playerInfo.Position.X, playerInfo.Position.Y].Piece == null)
                return true;

            return false;
        }

        public bool Validate(TestPiece testPiece)
        {
            var playerInfo = _gameState.GetPlayer(testPiece.playerGuid);

            if (playerInfo.HasPiece)
                return true;

            return false;
        }

        public bool Validate(DestroyPiece destroyPiece)
        {
            var playerInfo = _gameState.GetPlayer(destroyPiece.playerGuid);

            if (playerInfo.HasPiece)
                return true;

            return false;
        }

        public bool Validate(JoinGame joinGame, JoinGameValidationScope validationScope)
        {
            var desiredTeam = joinGame.preferredTeam == MessageTeamColour.blue ? _gameState.TeamBlue : _gameState.TeamRed;

            if(validationScope == JoinGameValidationScope.Team)
                return ValidateJoinGameTeam(desiredTeam);
            else if(validationScope == JoinGameValidationScope.Role)
                return ValidateJoinGameRole(joinGame.preferredRole);
            else
                return ValidateJoinGameBoth(desiredTeam, joinGame.preferredRole);
        }

        private bool ValidateJoinGameTeam(List<PlayerInfo> desiredTeam)
        {
            if(desiredTeam.Count >= _numberOfPlayersPerTeam)
                return false;
            return true;
        }

        private bool ValidateJoinGameRole(PlayerType role)
        {
            if(role == PlayerType.leader)
            {
                if(_gameState.TeamBlue.Exists(p => p.IsLeader) && _gameState.TeamRed.Exists(p => p.IsLeader))
                    return false;
            }
            else
            {
                if(!TeamNeedsPlayer(_gameState.TeamBlue) && !TeamNeedsPlayer(_gameState.TeamRed))
                    return false;
            }
            return true;
        }

        private bool ValidateJoinGameBoth(List<PlayerInfo> desiredTeam, PlayerType role)
        {
            if(role == PlayerType.leader)
            {
                if(desiredTeam.Exists(p => p.IsLeader))
                    return false;
            }
            else
            {
                if(!TeamNeedsPlayer(desiredTeam))
                    return false;
            }
            return true;
        }

        private bool TeamNeedsPlayer(List<PlayerInfo> team)
        {
            if(team.Count >= _numberOfPlayersPerTeam)
                return false;
            if(team.Count == _numberOfPlayersPerTeam - 1 && !team.Exists(p => p.IsLeader))
                return false;
            return true;
        }


        private bool IsValidPlayer(ulong id)
        {
            if (_gameState.TeamBlue.Exists(p => p.Id == id))
                return true;
            if (_gameState.TeamRed.Exists(p => p.Id == id))
                return true;
            return false;
        }

        public bool Validate(AuthorizeKnowledgeExchange authorizeKnowledgeExchange)
        {
            return IsValidPlayer(authorizeKnowledgeExchange.withPlayerId);
        }

        public bool Validate(RejectKnowledgeExchange rejectKnowledgeExchange)
        {
            var senderPlayerInfo = _gameState.GetPlayer(rejectKnowledgeExchange.senderPlayerId);
            var receiverPlayerInfo = _gameState.GetPlayer(rejectKnowledgeExchange.playerId);

            if (!IsValidPlayer(receiverPlayerInfo.Id))
                return false;
            if (senderPlayerInfo.Color == receiverPlayerInfo.Color && receiverPlayerInfo.IsLeader)
                return false;

            return true;
        }

        public bool Validate(Data data)
        {
            return IsValidPlayer(data.playerId);
        }
    }
}
