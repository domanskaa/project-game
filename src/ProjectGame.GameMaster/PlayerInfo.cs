﻿using ProjectGame.Core.Enum;
using ProjectGame.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;


namespace ProjectGame.GameMaster
{
    public class PlayerInfo : PlayerMapInfo
    {
        public Guid Guid { get; set; }
        public Position Position { get; set; }
        public bool HasPiece => Piece != null;
        public bool IsLeader { get; set; }
    }
}
