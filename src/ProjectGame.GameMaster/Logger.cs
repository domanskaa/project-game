﻿using ProjectGame.Core.Enum;
using ProjectGame.Core.Models.Messages;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ProjectGame.GameMaster
{
    public class Logger
    {
        private static readonly object _mutex = new object();
        private string _filename;
        public Logger(string filename)
        {
            _filename = filename;
        }

        public void LogRequest(string type, ulong gameId, ulong playerId, Guid playerGuid, PlayerColor color, PlayerType role)
        {
            Task.Run(() => { 
                lock (_mutex)
                {
                    var colString = color == PlayerColor.Blue ? "blue" : "red";
                    var rolString = role == PlayerType.leader ? "leader" : "member";
                    var csvRow = $"{type};{DateTime.Now.ToString("MM/dd/yyyy h:mm tt")};{gameId};{playerId};{colString};{rolString}\n";
                    File.AppendAllText(_filename, csvRow);
                }
            });
        }
    }
}
