﻿using ProjectGame.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectGame.GameMaster
{
    public static class PieceFactory
    {
        private static ulong _idCounter = 0;
        public static Piece GetNewInstance(bool isSham)
        {
            _idCounter++;
            return new Piece(_idCounter, isSham, false);
        }
    }
}
