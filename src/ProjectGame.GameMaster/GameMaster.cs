﻿using ProjectGame.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.PlayerProject;
using ProjectGame.Core.Models.Configuration;
using ProjectGame.Core.Enum;
using System.Linq;
using ProjectGame.GameMaster;
using ProjectGame.Core.Models.Messages;
using ProjectGame.GameMaster.Exceptions;
using ProjectGame.Core.Interfaces;
using ProjectGame.GameMaster.Enum;
using ProjectGame.GameMaster.Converters;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.IO;

namespace ProjectGame.GameMaster
{
    public class GameMaster
    {
        private GameState _gameState;
        private GameMasterSettings _gameMasterSettings;
        private IGameMasterAdapter _adapter;
        private ActionValidator _validator;
        private Random _rnd;
        private ulong _gameId;
        private Logger _logger;
        private bool gameWasFinished;
        private ConcurrentQueue<Action> _pendingActionsQueue;

        private HttpClient _httpClient;
        private Thread _putNewPiecesThread;

        public GameMaster(GameMasterSettings gameMasterSettings, IGameMasterAdapter gameMasterAdapter)
        {
            this._pendingActionsQueue = new ConcurrentQueue<Action>();
            this._gameMasterSettings = gameMasterSettings;
            this._httpClient = new HttpClient();
            this._adapter = new AdapterDelayer(gameMasterAdapter, gameMasterSettings);
            this._logger = new Logger($"GameMaster-log-{DateTime.Now.ToString("MM-dd-yyyy")}.csv");
            this._rnd = new Random();
            _gameState = new GameState()
            {
                TeamBlue = new List<PlayerInfo>(),
                TeamRed = new List<PlayerInfo>(),
                IsGameFinished = false,
            };

            InitializeBoard();

            _validator = new ActionValidator(_gameState, (int)_gameMasterSettings.GameDefinition.NumberOfPlayersPerTeam);

            InitializeGame();
            StartGame();
            MainLoop();
        }

        private void MainLoop()
        {
            while (true)
            {
                ProcessPendingActionsQueue();

                SendStateToUI();
                object msg = _adapter.Receive();
                LogMessage(msg);
                Handle(msg as dynamic);
            }
        }

        private void LogMessage(object msg)
        {
            GameMessage gameMessage = msg as GameMessage;
            if (gameMessage != null)
            {
                PlayerInfo playerInfo = _gameState.GetPlayer(gameMessage.playerGuid);
                _logger.LogRequest((msg as dynamic).ToString(), _gameId, playerInfo.Id, playerInfo.Guid, playerInfo.Color,
                    playerInfo.IsLeader ? PlayerType.leader : PlayerType.member);
            }
        }

        private void ProcessPendingActionsQueue()
        {
            Action pendingAction;
            do
            {
                pendingAction = null;
                if (_pendingActionsQueue.TryDequeue(out pendingAction))
                    pendingAction();
            } while (pendingAction != null);
        }

        private void SendStateToUI()
        {
            //if((new Random()).Next()%100 <= 1)
                _httpClient.PostAsync("http://127.0.0.1:8081/gm", new JsonContent(_gameState.Board));
        }

        private void Handle(Move move)
        {
            PlayerInfo playerInfo = _gameState.GetPlayer(move.playerGuid);
            Field oldField = _gameState.Board.GetField(playerInfo.Position);
            Field newField = oldField;
            PieceDto[] pieces = null;
            TaskFieldDto[] taskFields = null;
            GoalFieldDto[] goalFields = null;
            Position newPosition = new Position(playerInfo.Position.X, playerInfo.Position.Y);

            switch (move.direction)
            {
                case MoveType.down:
                    newPosition.Y--;
                    break;
                case MoveType.up:
                    newPosition.Y++;
                    break;
                case MoveType.left:
                    newPosition.X--;
                    break;
                case MoveType.right:
                    newPosition.X++;
                    break;
            }

            if (_validator.Validate(move, MoveValidationScope.Both))
            {
                oldField.Player = null;

                playerInfo.Position = newPosition;
                newField = _gameState.Board.GetField(playerInfo.Position);
                newField.Player = playerInfo;
            }
            else if (!_validator.Validate(move, MoveValidationScope.Player))
            {
                newField = _gameState.Board.GetField(newPosition);
            }

            if (newField != oldField)
            {

                if (!newField.InGoalArea)
                {
                    taskFields = new TaskFieldDto[]{
                        newField.ToTaskFieldDto(),
                    };
                }
                else
                {
                    goalFields = new GoalFieldDto[]{
                        newField.ToGoalFieldDto(),
                    };
                }

                if (newField.Piece != null)
                {
                    pieces = new PieceDto[]{
                        newField.Piece.ToPieceDto(),
                    };
                }
            }

            Data data = new Data()
            {
                TaskFields = taskFields,
                GoalFields = goalFields,
                Pieces = pieces,
                PlayerLocation = playerInfo.Position.ToLocationDto(),
                gameFinished = _gameState.IsGameFinished,
                playerId = playerInfo.Id
            };
            _adapter.MakeMoveResponseRequest(data);
        }

        private void Handle(PlayerDisconnected playerDisconnected)
        {

        }
        private void Handle(Discover discover)
        {
            PlayerInfo playerInfo = _gameState.GetPlayer(discover.playerGuid);
            List<TaskFieldDto> taskFields = new List<TaskFieldDto>();
            List<GoalFieldDto> goalFields = new List<GoalFieldDto>();
            List<PieceDto> pieces = new List<PieceDto>();

            int lowX = Math.Max(playerInfo.Position.X - 1, 0);
            int lowY = Math.Max(playerInfo.Position.Y - 1, 0);
            int highX = Math.Min(playerInfo.Position.X + 2, (int)_gameState.Board.BoardWidth);
            int highY = Math.Min(playerInfo.Position.Y + 2, (int)_gameState.Board.BoardHeight);
            if (playerInfo.Color == PlayerColor.Blue)
            {
                highY = Math.Min(highY, (int)(_gameState.Board.GoalAreaHeight + _gameState.Board.TaskAreaHeight - 1));
            }
            else
            {
                lowY = Math.Max(lowY, (int)(_gameState.Board.GoalAreaHeight));
            }

            for (int i = lowX; i < highX; i++)
            {
                for (int j = lowY; j < highY; j++)
                {
                    Field currentField = _gameState.Board.Fields[i, j];
                    if (currentField.Piece != null)
                    {
                        pieces.Add(currentField.Piece.ToPieceDto());
                    }
                    if (currentField.InGoalArea)
                    {
                        goalFields.Add(currentField.ToGoalFieldDto());
                    }
                    else
                    {
                        taskFields.Add(currentField.ToTaskFieldDto());
                    }
                }
            }
            Data data = new Data()
            {
                TaskFields = taskFields.ToArray(),
                GoalFields = goalFields.ToArray(),
                Pieces = pieces.ToArray(),
                PlayerLocation = playerInfo.Position.ToLocationDto(),
                playerId = playerInfo.Id,
                gameFinished = _gameState.IsGameFinished
            };
            _adapter.MakeDiscoverResponseRequest(data);
        }
        private void Handle(PickUpPiece pickUpPiece)
        {
            PlayerInfo playerInfo = _gameState.GetPlayer(pickUpPiece.playerGuid);
            Field field = _gameState.Board.GetField(playerInfo.Position);
            PieceDto[] pieces = null;

            if (_validator.Validate(pickUpPiece))
            {
                pieces = new PieceDto[]{
                    field.Piece.ToPieceDto()
                };
                playerInfo.Piece = field.Piece;
                field.Piece.PlayerId = playerInfo.Id;
                field.Piece = null;
                RecalculatePiecesManhattanDistance();
            }

            Data data = new Data()
            {
                Pieces = pieces,
                PlayerLocation = playerInfo.Position.ToLocationDto(),
                playerId = playerInfo.Id,
                gameFinished = _gameState.IsGameFinished
            };

            _adapter.MakePickUpResponseRequest(data);
        }
        private void Handle(PlacePiece placePiece)
        {
            PlayerInfo playerInfo = _gameState.GetPlayer(placePiece.playerGuid);
            Field field = _gameState.Board.GetField(playerInfo.Position);
            Piece piece = playerInfo.Piece;
            GoalFieldDto goalField = field.ToGoalFieldDto();
            TaskFieldDto taskField = field.ToTaskFieldDto();
            Data data = new Data()
            {
                gameFinished = _gameState.IsGameFinished,
                playerId = playerInfo.Id,
                PlayerLocation = playerInfo.Position.ToLocationDto(),
            };
            if (_validator.Validate(placePiece))
            {
                if (field.InGoalArea)
                {
                    playerInfo.Piece = null;
                    if (piece.IsSham)
                        goalField.type = Core.Models.Messages.GoalFieldType.unknown;
                    else
                    {
                        if (field.Type == FieldType.GoalNotDiscovered)
                        {
                            CompleteTask(field);
                            goalField.type = Core.Models.Messages.GoalFieldType.goal;
                        }
                        else
                        {
                            goalField.type = Core.Models.Messages.GoalFieldType.nongoal;
                        }
                    }
                    data.GoalFields = new GoalFieldDto[] { goalField };
                }
                else
                {
                    if (field.Piece == null)
                    {
                        field.Piece = playerInfo.Piece;
                        playerInfo.Piece = null;
                    }
                    taskField = field.ToTaskFieldDto();
                    data.TaskFields = new TaskFieldDto[] { taskField };
                }
            }

            
            _adapter.MakePlaceResponseRequest(data);
        }
        private void Handle(DestroyPiece destroyPiece)
        {
            PlayerInfo playerInfo = _gameState.GetPlayer(destroyPiece.playerGuid);
            Field field = _gameState.Board.GetField(playerInfo.Position);
            Data data = new Data()
            {
                gameFinished = _gameState.IsGameFinished,
                playerId = playerInfo.Id,
                PlayerLocation = playerInfo.Position.ToLocationDto(),
            };

            if (_validator.Validate(destroyPiece))
            {
                playerInfo.Piece = null;
                if (field.InGoalArea)
                    data.GoalFields = new GoalFieldDto[] { field.ToGoalFieldDto() };
                else
                    data.TaskFields = new TaskFieldDto[] { field.ToTaskFieldDto() };
            }
            _adapter.MakeDestroyResponseRequest(data);
        }
        private void Handle(TestPiece testPiece)
        {
            PlayerInfo playerInfo = _gameState.GetPlayer(testPiece.playerGuid);
            Field field = _gameState.Board.GetField(playerInfo.Position);
            Data data = new Data()
            {
                gameFinished = _gameState.IsGameFinished,
                playerId = playerInfo.Id,
                PlayerLocation = playerInfo.Position.ToLocationDto(),
            };

            if (_validator.Validate(testPiece))
            {
                PieceDto piece = playerInfo.Piece.ToPieceDto();
                piece.type = playerInfo.Piece.IsSham ? PieceType.sham : PieceType.normal;
                data.Pieces = new PieceDto[] { piece };
                playerInfo.Piece.IsTested = true;
            }
            _adapter.MakeDestroyResponseRequest(data);
        }
        private void Handle(AuthorizeKnowledgeExchange authorizeKnowledgeExchange)
        {

        }

        private void CompleteTask(Field field)
        {
            field.Type = FieldType.GoalCompleted;
            if (field.TeamOwnership == TeamOwnership.Blue)
                _gameState.TeamBlueCompletedTasksCount++;
            else
                _gameState.TeamRedCompletedTasksCount++;
            CheckWinCondition(field.TeamOwnership);
        }

        private void CheckWinCondition(TeamOwnership teamOwnership)
        {
            if (!gameWasFinished)
            {
                if (teamOwnership == TeamOwnership.Blue)
                {
                    if (_gameState.TeamBlueCompletedTasksCount == _gameMasterSettings.GameDefinition.Goals.Where(g => g.team == TeamColour.blue).Count())
                    {
                        _gameState.IsGameFinished = true;
                    }
                }

                else
                {
                    if (_gameState.TeamRedCompletedTasksCount == _gameMasterSettings.GameDefinition.Goals.Where(g => g.team == TeamColour.red).Count())
                    {
                        _gameState.IsGameFinished = true;
                    }
                }

                if (_gameState.IsGameFinished)
                {
                    FinishGame(teamOwnership);
                }
            }

        }
        private void FinishGame(TeamOwnership whoWon)
        {
            gameWasFinished = true;
            LogWinners(whoWon);
            SendGameFinished();
        }

        private void LogWinners(TeamOwnership teamColor)
        {
            PlayerColor winningColor = teamColor == TeamOwnership.Red ? PlayerColor.Red : PlayerColor.Blue;
            Console.WriteLine($"{winningColor} team won");
            foreach(PlayerInfo p in _gameState.Players)
            {
                var messageType = "defeat";
                if(winningColor == p.Color)
                {
                    messageType = "victory";
                }
                _logger.LogRequest(messageType, _gameId, p.Id, p.Guid, p.Color, p.IsLeader ? PlayerType.leader : PlayerType.member);
            }
        }

        private void SendGameFinished()
        {
            Data gameStarted = new Data()
            {
                gameFinished = true
            };
            _adapter.MakeGameFinishedRequest(gameStarted);
        }

        private void InitializeGame()
        {
            TryRegisterGame();
            WaitAndJoinPlayers();
        }

        private void StartGame()
        {
            List<PlayerDto> playerDtos = new List<PlayerDto>();
            Game gameStarted = new Game()
            {
                Board = new GameBoard(){
                    width = _gameState.Board.BoardWidth,
                    tasksHeight = _gameState.Board.TaskAreaHeight,
                    goalsHeight = _gameState.Board.GoalAreaHeight,
                }
            };
            foreach(PlayerInfo playerInfo in _gameState.Players)
            {
                playerDtos.Add(new PlayerDto(){
                    team = playerInfo.Color == PlayerColor.Blue ? MessageTeamColour.blue : MessageTeamColour.red,
                    type = playerInfo.IsLeader ? PlayerType.leader : PlayerType.member,
                    id = playerInfo.Id
                });
            }
            gameStarted.Players = playerDtos.ToArray();
            foreach(PlayerInfo playerInfo in _gameState.Players)
            {
                gameStarted.playerId = playerInfo.Id;
                gameStarted.PlayerLocation = playerInfo.Position.ToLocationDto();
                _adapter.MakeGameRequest(gameStarted);
            }

            
            _putNewPiecesThread = new Thread(() => PutNewPieces());
            _putNewPiecesThread.Start();
        }

        private void PutNewPieces()
        {
            while(!_gameState.IsGameFinished)
            {
                _pendingActionsQueue.Enqueue(() =>
                {
                    Position newPiecePosition = RandomCoordinatesFromArea(0, _gameState.Board.BoardWidth, _gameState.Board.GoalAreaHeight, _gameState.Board.GoalAreaHeight + _gameState.Board.TaskAreaHeight, 1).First();
                    if (_gameState.Board.GetField(newPiecePosition).Piece != null) // TODO: BUG: XXX: change to place 
                        return;
                    Piece pieceToPlace = PieceFactory.GetNewInstance(false);
                    if (_gameMasterSettings.GameDefinition.ShamProbability >= _rnd.NextDouble())
                        pieceToPlace.IsSham = true;
                    else
                        pieceToPlace.IsSham = false;

                    _gameState.Board.GetField(newPiecePosition).Piece = pieceToPlace;
                    RecalculatePiecesManhattanDistance();
                });

                Thread.Sleep((int)_gameMasterSettings.GameDefinition.PlacingNewPiecesFrequency);
                
            }
        }

        private void InitializeBoard()
        {
            _gameState.Board = new Board(_gameMasterSettings.GameDefinition.GoalAreaLength, _gameMasterSettings.GameDefinition.BoardWidth, _gameMasterSettings.GameDefinition.TaskAreaLength);
            _gameState.Board.InitializeFields();
            InitializePieces();
            InitializeGoals();

            RecalculatePiecesManhattanDistance();
        }

        private void TryRegisterGame()
        {
            RegisterGame registerGame = new RegisterGame()
            {
                NewGameInfo = new GameInfo()
                {
                    gameName = _gameMasterSettings.GameDefinition.GameName,
                    redTeamPlayers = _gameMasterSettings.GameDefinition.NumberOfPlayersPerTeam,
                    blueTeamPlayers = _gameMasterSettings.GameDefinition.NumberOfPlayersPerTeam,
                }
            };
            _adapter.MakeRegisterGameRequest(registerGame);

            object msg = _adapter.Receive();
            if (msg is ConfirmGameRegistration)
            {
                ConfirmGameRegistration confirmGameRegistration = msg as ConfirmGameRegistration;
                _gameId = confirmGameRegistration.gameId;
            }
            else if (msg is RejectGameRegistration)
            {
                throw new WrongConfiguration("Couldn't create game, probably the name is already in use");
            }
            else
            {
                throw new NotExpectedResponse($"Expected ConfirmJoiningGame or RejectJoiningGame, got {msg.ToString()}");
            }
        }
        private void WaitAndJoinPlayers()
        {
            uint playersPerTeam = _gameMasterSettings.GameDefinition.NumberOfPlayersPerTeam;

            // TODO: We assume that players initial positions are random
            List<Position> bluePlayersCoordinates =
                RandomCoordinatesFromArea(0, _gameState.Board.BoardWidth,
                0, _gameState.Board.GoalAreaHeight, _gameMasterSettings.GameDefinition.NumberOfPlayersPerTeam);

            List<Position> redPlayersCoordinates =
                RandomCoordinatesFromArea(0, _gameState.Board.BoardWidth,
                _gameState.Board.GoalAreaHeight + _gameState.Board.TaskAreaHeight, _gameState.Board.BoardHeight,
                 _gameMasterSettings.GameDefinition.NumberOfPlayersPerTeam);

            while (_gameState.TeamBlue.Count + _gameState.TeamRed.Count != 2 * playersPerTeam)
            {
                JoinGame msg = _adapter.Receive() as JoinGame;
                if (msg != null)
                {
                    // TODO: We assume that both preferences are required which is probably wrong assumption
                    // There is lack of specification in this area, the code that follows probably will be changed
                    if (_validator.Validate(msg, JoinGameValidationScope.Both))
                    {
                        PlayerInfo newPlayer = new PlayerInfo()
                        {
                            Guid = Guid.NewGuid(),
                        };

                        Position position;
                        List<Position> availablePlayersCoordinates = null;

                        if (msg.preferredRole == PlayerType.leader)
                            newPlayer.IsLeader = true;
                        else
                            newPlayer.IsLeader = false;

                        if (msg.preferredTeam == MessageTeamColour.blue)
                        {
                            _gameState.TeamBlue.Add(newPlayer);
                            newPlayer.Color = PlayerColor.Blue;

                            availablePlayersCoordinates = bluePlayersCoordinates;
                        }
                        else if (msg.preferredTeam == MessageTeamColour.red)
                        {
                            _gameState.TeamRed.Add(newPlayer);
                            newPlayer.Color = PlayerColor.Red;

                            availablePlayersCoordinates = redPlayersCoordinates;
                        }

                        position = availablePlayersCoordinates.Last();
                        availablePlayersCoordinates.RemoveAt(availablePlayersCoordinates.Count - 1);

                        newPlayer.Position = position;
                        newPlayer.Id = msg.playerId;

                        _gameState.Board.Fields[newPlayer.Position.X, newPlayer.Position.Y].Player = newPlayer;

                        ConfirmJoiningGame confirmJoiningGame = new ConfirmJoiningGame()
                        {
                            PlayerDefinition = new PlayerDto()
                            {
                                // From spec: "Id is assigned by a communication server when player wishes to join a game and that way passed to a game master"
                                id = msg.playerId,
                                team = msg.preferredTeam,
                                type = msg.preferredRole
                            },
                            gameId = _gameId,
                            privateGuid = newPlayer.Guid.ToString(),
                            playerId = msg.playerId,
                        };
                        _adapter.MakeConfirmJoiningGameRequest(confirmJoiningGame);
                    }
                    else
                    {
                        RejectJoiningGame rejectJoiningGame = new RejectJoiningGame()
                        {
                            gameName = _gameMasterSettings.GameDefinition.GameName,
                            playerId = msg.playerId
                        };
                        _adapter.MakeRejectJoiningGameRequest(rejectJoiningGame);
                    }
                }
            }
        }
        private void InitializeGoals()
        {
            List<GoalField> goalsCoordinates = _gameMasterSettings.GameDefinition.Goals.ToList();

            foreach (var goalField in goalsCoordinates)
            {
                _gameState.Board.Fields[goalField.x, goalField.y].Type = FieldType.GoalNotDiscovered;
            }
        }

        
        private void InitializePieces()
        {

            List<Position> initialPiecesCoordinates =
                RandomCoordinatesFromArea(0, _gameState.Board.BoardWidth,
                _gameState.Board.GoalAreaHeight, _gameState.Board.GoalAreaHeight + _gameState.Board.TaskAreaHeight,
                _gameMasterSettings.GameDefinition.InitialNumberOfPieces);


            foreach (var coordinates in initialPiecesCoordinates)
            {
                Piece pieceToPlace = PieceFactory.GetNewInstance(false);

                if (_gameMasterSettings.GameDefinition.ShamProbability >= _rnd.NextDouble())
                    pieceToPlace.IsSham = true;
                else
                    pieceToPlace.IsSham = false;

                _gameState.Board.Fields[coordinates.X, coordinates.Y].Piece = pieceToPlace;
            }
        }

        private List<Position> RandomCoordinatesFromArea(uint x1, uint x2, uint y1, uint y2, uint n)
        {
            if (x1 > x2 || y1 > y2)
                throw new ArgumentException();

            List<Position> coordinates = new List<Position>();
            for (uint i = x1; i < x2; i++)
            {
                for (uint j = y1; j < y2; j++)
                {
                    coordinates.Add(new Position((int)i, (int)j));
                }
            }
            List<Position> randomChosenCoordinates = coordinates.OrderBy(x => _rnd.Next()).Take((int)n).ToList();
            return randomChosenCoordinates;
        }

        private void RecalculatePiecesManhattanDistance()
        {
            List<Position> placedPieces = new List<Position>();
            for (int k = 0; k < _gameState.Board.BoardWidth; k++)
            {
                for (int l = 0; l < _gameState.Board.BoardHeight; l++)
                {
                    if (_gameState.Board.Fields[k, l].Piece != null)
                        placedPieces.Add(new Position(k, l));
                }
            }
            for (int i = 0; i < _gameState.Board.BoardWidth; i++)
            {
                for (int j = 0; j < _gameState.Board.BoardHeight; j++)
                {
                    int minDistance = int.MaxValue;
                    foreach (var pieceCoordinates in placedPieces)
                    {
                        int dist = ManhattanDistance(i, j, pieceCoordinates.X, pieceCoordinates.Y);
                        if (dist < minDistance)
                            minDistance = dist;
                    }
                    _gameState.Board.Fields[i, j].ManhattanDistance = minDistance;
                }
            }
        }

        private int ManhattanDistance(int x1, int y1, int x2, int y2)
        {
            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }


        public class JsonContent : StringContent
        {
            public JsonContent(object obj) :
                base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
            { }
        }
    }
}
