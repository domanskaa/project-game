using System;
using System.Collections.Generic;
using System.Text;
using ProjectGame.Core.Models.Messages;
using ProjectGame.Core.Models.Configuration;
using ProjectGame.Core.Enum;
using ProjectGame.Core.Models;
using ProjectGame.GameMaster.Enum;
using System.Linq;

namespace ProjectGame.GameMaster
{
    public static class ConfigurationValidator
    {
        public static bool Validate(GameMasterSettings settings, out string message)
        {
            bool isValid = true;
            string gameDefMessage;
            string nullPropMessage;

            if(settings == null)
            {
                message = "Game Master settings hasn't been read properly\n";
                return false;
            }
            if(IsAnyNull(settings, out nullPropMessage))
            {
                message = "Invalid Game Master settings. All properties have to be set.\n Unset properties:\n";
                message += nullPropMessage;
                return false;
            }
            isValid &= ValidateGameDefinition(settings.GameDefinition, out gameDefMessage);
            if(isValid)
                message = "Valid Game Master settings";
            else
                message = "Invalid Game Master settings. Error list:\n";
            message += gameDefMessage;

            return isValid;
        }

        private static bool ValidateGameDefinition(GameMasterSettingsGameDefinition gameDefinition, out string message)
        {
            bool isValid = true;
            message = "";
            string goalAreaMessage;

            if(gameDefinition.BoardWidth <= 0)
            {
                isValid = false;
                message += "-Board width has to be greater than 0\n";
            }
            if(gameDefinition.TaskAreaLength <= 0)
            {
                isValid = false;
                message += "-Task area length has to be greater than 0\n";
            }
            if(gameDefinition.NumberOfPlayersPerTeam <= 0)
            {
                isValid = false;
                message += "-Number of players per team has to be greater than 0\n";
            }
            if(gameDefinition.PlacingNewPiecesFrequency <= 0)
            {
                isValid = false;
                message += "-Placing new pieces frequency has to be greater than 0\n";
            }
            if(gameDefinition.ShamProbability < 0 || gameDefinition.ShamProbability > 1)
            {
                isValid = false;
                message += "-Sham probability has to be a number in range [0, 1]\n";
            }
            if(String.IsNullOrEmpty(gameDefinition.GameName))
            {
                isValid = false;
                message += "-Game name has to be non-empty string\n";
            }
            if(gameDefinition.InitialNumberOfPieces > gameDefinition.BoardWidth * gameDefinition.TaskAreaLength)
            {
                isValid = false;
                message += "-Initial number of pieces has to be less than task area fields count\n";
            }
            isValid &= ValidateGoalArea(gameDefinition, out goalAreaMessage);
            message += goalAreaMessage;
            
            return isValid;
        }

        private static bool ValidateGoalArea(GameMasterSettingsGameDefinition gameDefinition, out string message)
        {
            bool isValid = true;
            message = "";
            uint fieldsCount = gameDefinition.GoalAreaLength*gameDefinition.BoardWidth;
            GoalField[] blueGoals = gameDefinition.Goals.Where(g => g.team == TeamColour.blue).ToArray();
            GoalField[] redGoals = gameDefinition.Goals.Where(g => g.team == TeamColour.red).ToArray();

            if(gameDefinition.GoalAreaLength <= 0)
            {
                isValid = false;
                message += "-Goal area length has to be greater than 0\n";
            }
            if(fieldsCount < gameDefinition.NumberOfPlayersPerTeam)
            {
                isValid = false;
                message += "-Goal area too small to place players properly\n";
            }
            // XXX: We assume that number of players in each team is equal so number of goals
            // should be equal too.
            if(blueGoals.Length != redGoals.Length)
            {
                isValid = false;
                message += "-Each team should have equal amount of goals to complete\n";
            }
            if(Math.Max(blueGoals.Length, redGoals.Length) > fieldsCount)
            {
                isValid = false;
                message += "-Goal area too small to place goals properly\n";
            }
            foreach(GoalField goal in blueGoals)
            {
                if(goal.x < 0 || goal.x >= gameDefinition.BoardWidth || goal.y < 0 || goal.y >= gameDefinition.GoalAreaLength)
                {
                    isValid = false;
                    message += "-Blue team's goals should be placed in blue goal area\n";
                    break;
                }
                foreach(GoalField anotherGoal in blueGoals)
                {
                    if(anotherGoal != goal && anotherGoal.x == goal.x && anotherGoal.y == goal.y)
                    {
                        isValid = false;
                        message += "-Blue goals should be placed separately\n";
                        break;
                    }
                }
            }
            foreach(GoalField goal in redGoals)
            {
                if(goal.x < 0 || goal.x >= gameDefinition.BoardWidth || 
                    goal.y < gameDefinition.GoalAreaLength + gameDefinition.TaskAreaLength || 
                    goal.y >= 2 * gameDefinition.GoalAreaLength + gameDefinition.TaskAreaLength)
                {
                    isValid = false;
                    message += "-Red team's goals should be placed in red goal area\n";
                    break;
                }
                foreach(GoalField anotherGoal in redGoals)
                {
                    if(anotherGoal != goal && anotherGoal.x == goal.x && anotherGoal.y == goal.y)
                    {
                        isValid = false;
                        message += "-Red goals should be placed separately\n";
                        break;
                    }
                }
            }

            return isValid;          
        }

        private static bool IsAnyNull(object obj, out string message)
        {
            bool isAnyNull = false;
            message = "";

            foreach(var pi in obj.GetType().GetProperties())
            {             
                object value = pi.GetValue(obj);
                if(value == null)
                {
                    isAnyNull = true;
                    message += pi.Name + "\n";
                    continue;
                }
                
                if(pi.PropertyType.IsArray)
                {
                    int i = 0;
                    foreach(var item in value as Array)
                    {
                        if(!IsSimple(pi.PropertyType))
                        {
                            string innerMessage;
                            if(IsAnyNull(item, out innerMessage))
                            {
                                isAnyNull = true;
                                message += pi.Name + "[" + i.ToString() + "]\n{\n"+ innerMessage + "}\n";
                            }
                        }
                        i++;
                    }
                    continue;
                }
                if(!IsSimple(pi.PropertyType))
                {
                    string innerMessage;
                    isAnyNull |= IsAnyNull(value, out innerMessage);
                    message += innerMessage;
                }
            }

            return isAnyNull;
        }

        private static bool IsSimple(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                return IsSimple(type.GetGenericArguments()[0]);
            return type.IsPrimitive 
                || type.IsEnum
                || type.Equals(typeof(string))
                || type.Equals(typeof(decimal));
        }
    }
}