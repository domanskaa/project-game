using ProjectGame.Core.Interfaces;
using ProjectGame.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using ProjectGame.CommunicationServer.Interfaces;

namespace ProjectGame.GameMaster
{
    public class GameMasterNetAdapter : IGameMasterAdapter
    {
        ICommunicationClient _communicationClient;
        ISerializer _serializer;

        public GameMasterNetAdapter(ICommunicationClient communicationClient, ISerializer serializer)
        {
            _communicationClient = communicationClient;
            _serializer = serializer;
        }

        public void MakeConfirmJoiningGameRequest(ConfirmJoiningGame confirmJoiningGame)
        {
            MakeRequest(confirmJoiningGame, typeof(ConfirmJoiningGame));
        }

        public void MakeDestroyResponseRequest(Data destroyResponseData)
        {
            MakeRequest(destroyResponseData, typeof(Data));
        }

        public void MakeGameStartedRequest(GameStarted gameStarted)
        {
            MakeRequest(gameStarted, typeof(GameStarted));
        }

        public void MakeMoveResponseRequest(Data moveResponseData)
        {
            MakeRequest(moveResponseData, typeof(Data));
        }

        public void MakeDiscoverResponseRequest(Data discoverResponseData)
        {
            MakeRequest(discoverResponseData, typeof(Data));
        }

        public void MakePickUpResponseRequest(Data pickUpResponseData)
        {
            MakeRequest(pickUpResponseData, typeof(Data));
        }

        public void MakePlaceResponseRequest(Data placeResponseData)
        {
            MakeRequest(placeResponseData, typeof(Data));
        }

        public void MakeRegisterGameRequest(RegisterGame registerGame)
        {
            MakeRequest(registerGame, typeof(RegisterGame));
        }

        public void MakeRejectJoiningGameRequest(RejectJoiningGame rejectJoiningGame)
        {
            MakeRequest(rejectJoiningGame, typeof(RejectJoiningGame));
        }

        public void MakeRejectKnowledgeExchangeRequest(RejectKnowledgeExchange rejectKnowledgeExchange)
        {
            MakeRequest(rejectKnowledgeExchange, typeof(RejectKnowledgeExchange));
        }

        public void MakeTestReponseRequest(Data testResponseData)
        {
            MakeRequest(testResponseData, typeof(Data));
        }

        public void MakeGameFinishedRequest(Data gameFinished)
        {
            MakeRequest(gameFinished, typeof(Data));
        }
        
        public void MakeGameRequest(Game game)
        {
            MakeRequest(game, typeof(Game));
        }

        public object Receive()
        {
            var bytes = _communicationClient.Receive();
            return _serializer.Deserialize(bytes);
        }

        private void MakeRequest(object obj, Type type)
        {
            var bytes = _serializer.Serialize(obj, type);
            _communicationClient.Send(bytes);
        }

    }
}
